---
title: "Homework"
author: "Elena Pak"
date: "08 05 2021"
output: html_document
---
Для начала, импортируем следующие библиотеки, которые необходимы для дальнейшей работы.
```{r message=FALSE, warning=FALSE}
library(data.table)
library(plotly)
library(readxl)
library(ggplot2)
```
#Задание 1. Дома в Игре престолов.
##Импорт данных
Импортируем данные по ссылке с помощью функции `fread()`
```{r message=FALSE, warning=FALSE}
got_chars <- fread('https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv')
```

```{r message=FALSE, warning=FALSE}
str(got_chars)
```
##Чистка данных
На место пропущенных значений, которые отбираются посредством логической операции на месте первого аргумента, записывается в переменную `house` значение `Unknown` с помощью оператора `:=`. При этом значение изменяется в данной таблице.
```{r message=FALSE, warning=FALSE}
got_chars[house == '', house := 'Unknown']
str(got_chars)
```

##График численности кланов
Создадим из got_chars объект got_chars_bar, где будет статистика по количеству людей в клане, а также по количеству мужчин и женщин. Для этого воспользуемся агрегацией с помощью конструкции `by`, которая отвечает за применение операции отдельно по группам. Для начала, посчитаем общее количество людей в каждом доме и запишем результаты в переменную `pop`. И воспользуемся функцией `.N` для создания колонки таблицы с тем, чтобы посчитать количество людей в каждой группе.

```{r message=FALSE, warning=FALSE}
got_chars_bar = got_chars[, list(pop = .N), by = list(house)]
got_chars_bar
```
Далее, посчитаем количество людей в каждом доме отдельно по мужчинам и по женщинам. Так как группируется по двум переменным, обернем их названия в лист. 

```{r message=FALSE, warning=FALSE}
got_chars_bar1 = got_chars[, list(pop = .N), by = list(house,male)]
got_chars_bar1
```

Чтобы объединить все результаты в одну таблицу, приведем вторую таблицу к широкому формату, используя функцию `dcast()` из пакета `data.table`. Уникальное значение создается с помощью переменной `house` перед тильдой, а переменная `male` после тильды определяет по каким колонкам будут разбиваться строки из старой таблицы. Переменная `pop` определяет значения. которые будут записываться в этих новых колонках.
```{r message=FALSE, warning=FALSE}
got_chars_bar1_wide <- dcast(data=got_chars_bar1, house ~ male, value.var = "pop")
got_chars_bar1_wide
```
Переименуем столбцы с помощью функции `colnames` и индексацией номеров конолок.
```{r message=FALSE, warning=FALSE}
colnames(got_chars_bar1_wide)[2]<-'females'
colnames(got_chars_bar1_wide)[3]<-'males'
got_chars_bar1_wide
```
Теперь объединим таблицы в одну с помощью функции `merge()`, сопоставив строки двух таблиц по переменной `house`.
```{r message=FALSE, warning=FALSE}
got_chars_bar <- merge(x = got_chars_bar, y = got_chars_bar1_wide,  by = 'house', all = TRUE)
got_chars_bar
```

В полученной таблице `got_chars_bar` отсортируем строки по убыванию общей численности в клане и заменим пропуски на тег <br>, чтобы названия кланов писались с новой строчки для каждого слова. Для этого воспользуемся функцией `order()`, который выдаст индексы строк в указанном порядке (в данном случае зададим порядок с помощью аргумента `decreasing = TRUE') и отсортируем строки, отобрав их в соответствующем порядке.
```{r message=FALSE, warning=FALSE}
got_chars_bar <- got_chars_bar[order(pop, decreasing = TRUE),]
# делаем название клана в две строки
got_chars_bar[, house := gsub('\\s+', '<br>', house)]

# задаем сортировку домов
got_chars_bar[, house := factor(house, levels = house)]
got_chars_bar
```

Нарисуем график численности первых 10 по размеру кланов, отобрав из датасета первые 10 значений.
```{r message=FALSE, warning=FALSE}
plot_ly(got_chars_bar[1:10], x = ~house, y = ~pop, type='bar')
```

Добавим hover. Для этого сначала была создана текстовая переменная, в которую вписаны значения с помощью функции `paste()`.
```{r message=FALSE, warning=FALSE}
got_chars_bar[, my_hover := paste(
  'house =', house, '<br>',
  'total members=', pop, '<br>',
  'males = ', males, '<br>',
  'females =', females)]
```

Теперь созданная переменная используется в качестве ховера в аргументе `text` функции `plot_ly`.
```{r message=FALSE, warning=FALSE}
plot_ly(got_chars_bar[1:10], x = ~house, y = ~pop, type='bar', text = ~my_hover, hoverinfo = 'text')
```

Теперь необходимо отредактировать общие параметры графика. Для этого воспользуемся базовой функцией `layout()`. Чтобы не записывать график в отдельную переменную, воспользуемя оператором `%>%`. В аргумент `title` запишем название таблицы, а в аргумент xaxis/yaxis передадим паараметры и их значения для оси X/Y, обернув в лист.

```{r message=FALSE, warning=FALSE}
plot_ly(got_chars_bar[1:10], x = ~house, y = ~pop, type='bar', text = ~my_hover, hoverinfo = 'text') %>%
  layout(title = 'Численность домов в GOT', 
         xaxis=list(title = ''), 
         yaxis=list(title = ''))
```
#Задание 2. Агресивность персонажей в книгах о Гарри Поттере.

Сначала, импортируем данные из файла `aggressive_actions.xlsx`. Так как файл имеет формат xlsx, и необходимо прочитать только определенную страницу, то воспользуемся пакетом `readxl` и функцией `read_xlsx`, в параметр `sheet` укажем название листа, который необходимо считать.
```{r message=FALSE, warning=FALSE}
actions <- read_excel('../aggressive_actions.xlsx', sheet = 'DataSet')
```
```{r message=FALSE, warning=FALSE}
str(actions)
```


Так как импортируемая таблица имеет формат `tibble`, воспользуемся функцией `as.data.table`, чтобы сохранить в нужный формат. ##Чистка данных
```{r message=FALSE, warning=FALSE}
actions <- as.data.table(actions)
```
##Чистка данных
Исправим орфографическую ошибку: Ron Wealsey вместо Ron Weasley. Для этого, отберем строки, в которые есть ошибка и запишем исправленную версию.
```{r message=FALSE, warning=FALSE}
actions[Name == 'Ron Wealsey', Name := 'Ron Weasley']
actions[Name == 'Ron Weasley']
```

Далее, подготови данные для визуализации. Отсортируем по убыванию колонки `tot`, и возьмем первые 15 строк. Для этого воспользуемся функцией `order()`, который выдаст индексы строк в указанном порядке (в данном случае зададим порядок с помощью аргумента `decreasing = TRUE') и отберем строки в соответствующем порядке. Запишем полученную таблицу.
```{r message=FALSE, warning=FALSE}
actions <- actions[order(tot, decreasing = TRUE), ][1:15, ]
actions
```
```{r message=FALSE, warning=FALSE}
actions[, names_ordered := gsub('\\s+', '\n', Name)]
actions[, names_ordered := factor(names_ordered, levels = names_ordered)]
```
##График агресивности персонажей в книгах о Гарри Поттере
Для начала создадим переменную `my_label`, которая используется для группировки характеров, чтобы использовать эту группировку для выделения разным цветом лэйблов для каждой группы.
```{r message=FALSE, warning=FALSE}
actions[, my_label := 'other']
actions[Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley'), my_label := 'main']
actions[creature == 1, my_label := 'creature']
```
```{r message=FALSE, warning=FALSE}
ggplot(actions, aes(x = names_ordered, y = tot)) +
  geom_bar(stat = 'identity', color='grey85', fill = 'grey90') +
  theme_classic() +
  geom_text(aes(label = tot, colour=as.factor(my_label)), vjust = 2) +
  labs(title = 'Character aggression\nHarry Potter Universe',
       x = NULL,
       y = NULL,
       caption = 'pmsar2020_hw1_pak: barchart') +
  theme(plot.title = element_text(hjust = 0.5),
        plot.caption = element_text(colour = 'grey60', face = "italic"),
        legend.position = "none") +
  scale_colour_manual(values = c('main' = 'darkred', 'creature' = 'darkblue', 'other' = 'grey30'))
```

#Задание 3. Динамика среднегодовой температуры в период 1750-2015

```{r message=FALSE, warning=FALSE}
data <- fread('https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv')
```
```{r message=FALSE, warning=FALSE}
str(data)
```

Создадим переменную `year` и запишим в нее год, использовав функцию `year()`
```{r message=FALSE, warning=FALSE}
data[, year := year(data$dt)]
```

Для каждого года посчитаем среднюю температуру
```{r message=FALSE, warning=FALSE}
avg_temp = data[, list(temp_mn = mean(LandAverageTemperature, na.rm = TRUE)), by = year]
avg_temp
```

Создадим график
```{r message=FALSE, warning=FALSE}
ggplot(avg_temp, aes(x = year, y = temp_mn)) +
  geom_line() +
  geom_smooth(method = 'glm', color = 'darkred', se = FALSE, alpha = 0.1) +
  geom_smooth(method = 'loess', color = 'darkblue', se = FALSE, alpha = 0.1) +
  theme_classic() +
  labs(x = NULL,
       y = NULL,
       title = 'Climate change dynamics, 1750-2015') +
  lims(y = c(0, max(avg_temp$temp_mn))) +
  scale_x_continuous(breaks = c(1800, 1900, 2000)) +
  theme(plot.title = element_text(hjust = 0.5))
```
