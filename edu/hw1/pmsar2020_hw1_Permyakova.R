# ДЗ_1. Пермякова Валентина, МАР-201

# Сначала устанавливаем необходимые пакеты для работы
# Делаем коды с установкой комментариями, чтобы каждый раз пакеты не загружались
#install.packages('data.table')
library(data.table)

#install.packages('readxl') 
library(readxl)

#install.packages('ggplot2') 
library(ggplot2)

#install.packages('plotly') 
library(plotly)

# Задание №1.
# Импортируем данные
got_chars <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv'
  )

got_chars[house == '', house := 'Unknown']  # Записываем пустые названия домов как "неизвестные" в этот же столбик.
# Оператор := позволяет изменять объект на его месте же, не переписывая в новый, поэтому мы 
# создаем вектор новых значений и записываем его в старую колонку с тем же названием.
# Чистка данных
got_chars[age < 0] # Выводим все строки, в которых возраст меньше нуля, находим две строки с отриц. возрастом (cм. столбец S.No).
got_chars <-
  got_chars[-c(1685, 1869),] # Удаляем строки (вписываем номера строк с отриц.возрастом)из этого датасета, переписываем его в то же название.
got_chars[age < 0] # Проверим, удалились ли эти строки. Да, 0 строк с возрастом < 0.

# Считаем статистику по кол-ву мужчин и женщин в кланах
# Для этого (так как нужна группировка по нескольким переменным) 
# указываем нужные колонки через список:

got_chars_bar <-
  got_chars[male %in% c('0', '1'),  #Фильтруем датасет, чтобы были только строки со значениями 1, 0 в колонке male.
            list(n_chars = uniqueN(name)), #в группах по полу и названию клана (дома)считаем количество чел-к.
            by = list(male, house)]  #Результаты записываем в новый объект.

# Отсортируем данные по убыванию с помощью функции arrange и выводим только первые десять строк

got_chars_bar_arr <- arrange(got_chars_bar, desc(n_chars)) 

# Делаем название клана в две строки
got_chars_bar_arr[, house := gsub("\\s+", " \n " , house)] #вставляем символ перевода строки - " \n " вместо <br>

# Задаем сортировку домов
got_chars_bar_arr[, house := factor(house, levels = unique(house))]
got_chars_bar_arr[1:4] #Выводим несколько строк таблицы, чтобы проверить правильность

#Строим столбчатую диаграмму (барчат)
ggplot(data = got_chars_bar_arr, aes(x = house, y = n_chars)) +   #задаем оси x, y
  geom_bar(colour = "green",
           #Выбираем геом для столбчатой диаграммы и цвета заливки и границ столбцов
           fill = "green",
           stat = "identity") +   #выбраем stat_identity, так как команда оставляет значения "у" неизменными.
  coord_cartesian(xlim = c(1, 9.95)) + #выводим только первые 10 баров (нельзя поставить 1, 10 - тк начинает виднеться 11 столбец)
  labs(x = "Название дома", #Задаем названия осей и название графика
       y = "Кол-во человек",
       title = "Численность домов в GoT") +
  theme_set(theme_light()) +  #задаем тему, отличную от классической
  theme(plot.title = element_text(hjust = 0.5)) #Центрируем заголовок графика



#Задание №2
path <-
  'C:\\R\\aggressive_actions.xlsx' #Создаем переменную пути файла, чтобы сократить запись кода  в дальнейшем
actions <- read_xlsx(path,
                     sheet = 'DataSet') #Загружаем базу, указываем название листа, который необходимо загрузить
actions <-
  as.data.table(actions) #делаем таблицу объектом data.table

actions[4, "Name"] <-
  "Ron Weasley" #Обращаемся к 4 строке столбца Name и перезаписываем в эту же строку новое имя

actions <- arrange(actions, desc(tot))  #Сортируем по убыванию
names_ordered = actions[1:15, ] #берем первые 15 строк

actions[, names_ordered := gsub('\\s+', ' \n ', Name)] #  Делим имена на две строки
actions[, names_ordered := factor(names_ordered, levels = names_ordered)] #Сортируем

ggplot(data = actions[1:15], aes(x = names_ordered, y = tot)) +
  geom_bar(colour = "grey85",
           fill = "grey90",
           stat = "identity") +
  geom_text(
    show.legend = FALSE,
    #Легенда не нужна
    nudge_y = -6,
    #настраиваем подписи к барам: nudge_y - отступ от основного положения подписи
    aes(label = tot, colour = ifelse(
      Name %in% c("Harry", "Hermione Granger", "Ron Weasley"),
      "1",
      ifelse(creature == 1, "2", "3")   # проверяем, равняется ли столбец "creature" 1, а "2" и "3" - это  группы, в соотв. с которыми красим график
    )),
    size = 3  #задаем размер подписей данных
  ) +
  #в colour разделяем все данные на три типа: Золотое Трио, тварей и остальных
  scale_color_manual(values = c("darkred", "darkblue", "grey30")) + #располагаем цвета в том порядке, в котором они должны быть
  labs(
    x = "",
    #задаем пустые названия осей
    y = "",
    title = "Characters aggression \n Harry Potter Universe",
    caption = "pmsar2020_hw1_permyakova: barchart"
  ) +
  theme(
    plot.title = element_text(hjust = 0.5),  #hjust нужен для централизации заголовка
    plot.caption = element_text(face = "italic", color = 'grey60', hjust = 1) #смещение вправо задали единицей
  ) 



#Задание №3
temp_data <-
  fread (
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv'
  )

year <-
  format(as.Date(temp_data$dt, format = "%d/%m/%Y"), "%Y") #меняем дату вида %d/%m/%Y на %Y
temp_data[, dt := year] #меняем даты
year = unique(year) #берем только уникальные

temp_mn = array(tapply(temp_data$LandAverageTemperature, temp_data$dt, mean, na.rm = TRUE)) #находим среднее значение температуры по годам

temp_data_new = data.frame(year, temp_mn) #новый дф, по которому будем строить

ggplot(data = temp_data_new, aes(x = year, y = temp_mn, group = 1)) +
  geom_line() +
  labs(x = "",
       y = "",
       title = "Climat change dynamics, 1750-2015") +
  theme(plot.title = element_text(hjust = 0.5)) +
  coord_cartesian(ylim = c(0, 10)) + #устанавливаем ограничения по y
  scale_x_discrete(breaks = c(1800, 1900, 2000)) + #ограничения на вывод по оси x
  geom_smooth(colour = "darkblue") +
  geom_smooth(method = 'glm',
              colour = "darkred",
              size = 1)
