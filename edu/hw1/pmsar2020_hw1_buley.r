# HW 1 Булей Полина
# Задание 1

#Загрузка необходимых для задания пакетов
library(data.table)
library(ggplot2)

#Загрузка массива данных
got_chars <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv'
  )

#Замена в столбике 'house' тех строк, где значения отсутствуют
got_chars[house == '', house := 'Unknown']

#Удаление строк, в которых возраст отрицательный. Поскольку при got_chars[-(age<0)] выбираются и NA, то необходимо указать через ИЛИ два условия, от обратного: возраст больше нуля или в ячейке нет данных
got_chars <- got_chars[(age > 0 | is.na(age) == TRUE)]

#Создание новой таблицы, где будет собрана численность домов общая, по полам, отфильтрованная в зависимости от дома
got_chars_bar <-
  got_chars[, list(
    total_house = .N,
    total_male = sum(male),
    total_female = sum(male == 0)
  ), by = house]

#Сортировка по убыванию
got_chars_bar <-
  got_chars_bar[order(total_house, decreasing = TRUE)]

# делаем название клана в две строки
got_chars_bar[, house := gsub('\\s+', '<br>', house)]

# задаем сортировку домов
got_chars_bar[, house := factor(house, levels = house)]

#Отбор 10 первых строк с самыми большими домами
got_chars_bar <- got_chars_bar[1:10]

#Построение графика. Цвет заливки синий, обводка столбцов красным, stat = 'identity', поэтому указываем обе оси X и Y
ggplot(got_chars_bar, aes(x = house, y = total_house)) +
  geom_bar(stat = 'identity', color = 'red', fill = 'blue')




# Задание 2

#Загрузка пакета
library(readxl)

#Загрузка данных из файла с указанием папки
actions <-
  read_xlsx(path = 'C:/Users/Polina/Downloads/aggressive_actions.xlsx', sheet = 2)

#Проверка класса объекта - data.frame
str(actions)

#data.frame лучше привести к формату data.table
actions <- as.data.table(actions)

#Проверка снова класса объекта - data.table
str(actions)

#Сортировка переменной tot по убыванию
actions <- actions[order(tot, decreasing = TRUE)]

#Отбор первых 15 строк
actions <- actions[1:15]

actions[, names_ordered := gsub('\\s+', '\n', Name)]

actions[, names_ordered := factor(names_ordered, levels = names_ordered)]

#Установка темы classic (белый фон для графика)
theme_set(theme_classic())

#Построение графика
#Значение указано для двух осей
#Указание смещения и откуда брать названия для колонок прописывается в geom_text
#Далее настройка визуала в theme
ggplot(actions, aes(x = names_ordered, y = tot)) +
  geom_bar(stat = 'identity', color = 'grey85', fill = 'grey90') +
  labs(
    title = 'Character agression \n Harry Potter Universe',
    x = '',
    y = '',
    caption = 'pmsar2020_hw1_Buley: barchart'
  ) +
  geom_text(label = actions$tot,
            nudge_x = 0,
            nudge_y = -4.5) +
  theme(
    plot.title = element_text(hjust = 0.5, size = 12),
    plot.caption = element_text(color = 'grey60', face = 'italic'),
    axis.text = element_text(size = 7)
  )




# Задание 3


#Загрузка базы
temperature <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv'
  )


#Создание колонки с годами
temperature[, Year := year(dt)]

#Создание temperature_2 с колонками, обозначающими среднюю годовую температуру и год
temperature_2 <-
  temperature[, list(temp_mn = mean(LandAverageTemperature, na.rm = TRUE)), by = Year]


#Построение графика типа geom_line + добавление методов линий сглаживания glm, loess
ggplot(data = temperature_2, mapping = aes(x = Year, y = temp_mn)) +
  geom_line() +
  geom_smooth(
    method = 'glm',
    color = 'darkred',
    se = F,
    size = 1.5
  ) +
  geom_smooth(method = 'loess',
              color = 'darkblue',
              alpha = 0.1) +
  scale_y_continuous(limits = c(0, 10)) +
  labs(
    title = 'Clymate change dynamics, 1750-2015',
    x = '',
    y = '',
    caption = 'pmsar2020_hw1_buley: line+smooth'
  ) +
  theme(plot.title = element_text(hjust = 0.5))
