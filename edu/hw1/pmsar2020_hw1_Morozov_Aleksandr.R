library(data.table)
library(ggplot2)
library(plotly)
library(readxl)


# ������� 1 ---------------------------------------------------------------
#��� 0
#��������� � ����� 
got_chars <- fread('character-predictions_pose.csv') 
#��� 1
#������ ������- ����������� ��������� ������������� �������� unknown
got_chars[house == '', house := 'Unknown']
#������� ������������� �������� �������� ��� ������  is.na
got_chars <- got_chars[age >= 0 | is.na(got_chars$age)]

#��� 2
#������� ����������� �� ������������ ����� � ����� � ���������� ������ � ������
#list ��������� ��� �������� ������ � ��������� (�������� ������)
#N-����� ����� ����� � ������, male- �������, �������������� ����� ������
#����� ����� N-male
got_chars_bar <- got_chars[,list (
  tot = .N,
  male = sum(male),
  female = .N - sum(male)),
  by = house]
got_chars_bar

#��� 3
# ��� ���������� �������� �������� ������ 10 ����� �� �����������
#order(-tot) ���������� ��� �������� �� �������� 
got_chars_bar <- got_chars_bar[order(-tot)]
got_chars_bar <- got_chars_bar[1:10, ]
# ������ �������� ����� � ��� ������
got_chars_bar <- got_chars_bar[, house := gsub('\\s+', '<br>', house)]
# ������ ���������� �����
got_chars_bar <- got_chars_bar[, house := factor(house, levels = house)]

#���4
#��������� hover � ����������� � �������� �����, ���������� ������ � �����, 
#���������� ������ � ���������� ������ � �����
got_chars_bar[, my_hover := paste(
  'total members =', tot, '<br>',
  'males =', male, '<br>',
  'females =', female)]

#������ ������� �� ���������� ������ � �����, ���������  ����� plotly
plot_ly(got_chars_bar, x = ~house, y = ~tot, type = 'bar',
        text = ~my_hover, hoverinfo = 'text')%>%
  layout(title = '����������� ����� � GoT',
         xaxis = list(title = ''),
         yaxis = list(title = ''))


# ������� 2 ---------------------------------------------------------------
#��� 0
#��������� � ����� � � ����������� �����
actions <- read_xlsx('aggressive_actions.xlsx', sheet = 'DataSet')

#��� 1
#������ ��� ������� 
actions <- as.data.table(actions)

#��� 2
#���������� ��������������� ������ 
actions[Name == 'Ron Wealsey', Name := 'Ron Weasley']

#��� 3
#��������� �� ��������
actions <- actions[order(-tot)]
#������� ������ 15 �����
actions <- actions[1:15]

#��� 4
#��� ������ gsub ������������ ������ ���������� 
actions[, names_ordered := gsub('\\s+', '\n', Name)]
actions[, names_ordered := factor(names_ordered, levels = names_ordered)]

#��� 5
#�� ������� �������� ������ ������ �� ��� �, ����� ���� �������..
#�������� ,(g+theme(axis.text=element_text((size=..), �� �������..
ggplot(data = actions,
       mapping = aes(x = names_ordered, y = tot)) +
  geom_bar(stat = "identity",
           fill = "grey90",
           color = 'grey85') +
  geom_text(data = actions[Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley')],
            aes(label = tot),
            color = 'darkred',
            vjust = 1.1) +
  geom_text(data = actions[creature == 1],
            aes(label = tot),
            color = 'darkblue',
            vjust = 1.1) +
  geom_text(data = actions[!Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley') &
                             creature == 0],
            aes(label = tot),
            color = 'grey30',
            vjust = 1.1) +
  labs(
    title = 'Character aggression\nHarry Potter Universe',
    x = '',
    y = '',
    caption = 'pmsar2020_hw1_morozov: barchart'
  ) +
  theme_classic() +
  theme(
    plot.title = element_text(face = 'italic', size = 8, hjust = 0.5),
    plot.caption = element_text(
      colour = 'grey60',
      face = 'italic',
      hjust = 0.9,
    )
  )



# ������� 3 ---------------------------------------------------------------
#��� 0
#��������� � ����� (����� �� ���������, ������ dt1)
dt1 <- fread('global_temperature.csv')  

#��� 1
# ������ ���������� ����, �������� � �� ��������  
dt1[,year := year(dt)]
#������� ������� ����������� �� ������� ���� 
#temp_mn- ������� ����������� �� ���, ������� ����������� �������� ����� ����������� 
dt1 <- dt1[, list(temp_mn = mean (LandAverageTemperature, na.rm = TRUE)), by = year ]

#��� 2
#������  ������ ������������� �����������.
#alpha-������������ ���������= 0,1
#glm � loess-�������� ��������� � ��������� �������������� ��������� ��������������
#������  ������ ������������� ����������� 
ggplot(data = dt1, mapping = aes(x = year, y = temp_mn)) +
  geom_line()+
  geom_smooth(method = 'glm', color = 'darkred', fill = 'darkred', alpha = 0.1) + 
  geom_smooth(method = 'loess', color = 'darkblue', fill = 'darkblue', alpha = 0.1) +
  labs(title= "Climat change dynamics, 1750 - 2015",
       caption  = "pmsar2020_hw1_�������: barchart",
       xaxis = list(title = ''),
       yaxis = list(title = ''))+
  labs ( x = "" ,y="")+
  theme(plot.title = element_text(hjust = 0,7),
        plot.caption = element_text(hjust = 1, face= "italic", color = 'grey60'))
#��������� �������� ������� �� ����� 'y' � ������, ��� ��� �� ������� � �������
#��� ������ scale_y_continuous(breaks = c(0, 2.5, 5.0, 7.5, 10.0))
#�� ���-�� ����� �� ���.
# -------------------------------------------------------------------------