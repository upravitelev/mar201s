# Загружаем пакеты -------------------------------------------------------

library(data.table)
library(readxl)
library(ggplot2)
library(plotly)
# Задание 1 ----------------------------------------------------------------

# Импортируем датасет
got_chars <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv'
  )

got_chars[house == '', house := 'Unknown'] # Данное выражение работает следующим образом:
# просматривается столбец "house " и тем строкам, которым соотвествует пустое значение (''), присваевается значение 'Unknown'

# Удаляем строки, в которых возраст отрицательный
got_chars <- got_chars[age >= 0 | is.na(got_chars$age)]

# Создаем объект, где будет статистика по количеству людей в клане, а также по количеству мужчин и женщин
got_chars_bar <-
  got_chars[, list(
    number_of_chars = .N,
    male = sum(male),
    female = .N - sum(male)
  ),
  by = house]

# Возьмем первые 10 домов по численности, включая Unknown:

got_chars_bar <- got_chars_bar[order(-number_of_chars)] # сортируем от большего к меньшему. Для этого ставим знак "-" перед "number_of_chars"

got_chars_bar <- got_chars_bar[1:10] # Оставляем 10 самых многочисленных домов

# Делаем название клана в две строки
got_chars_bar[, house := gsub('\\s+', '<br>', house)] # что-то пошло не так...

# Задаем сортировку домов
got_chars_bar[, house := factor(house, levels = house)]

# Рисуем диаграмму по количеству членов в клане:
# добавляем ховер с информацией о названии клана, количестве членов в клане, количестве мужчин и количестве женщин в клане
got_chars_bar[, hover := paste('number of members =',
                               number_of_chars,
                               'males =',
                               male,
                               'females =',
                               female)]
plot_ly(
  got_chars_bar,
  x = ~ house,
  y = ~ number_of_chars,
  type = 'bar',
  text = ~ hover,
  hoverinfo = 'text'
) %>%
  layout(
    title = 'Дома в "Игре престолов"',
    xaxis = list(title = 'Дом'),
    yaxis = list(title = 'Численность дома')
  )

# Задание 2 ----------------------------------------------------------------

# Открываем экселевский файл, данные с листа DataSet
actions <- read_xlsx('aggressive_actions.xlsx', sheet = 'DataSet')

# Переводим в формат таблицы
actions <- as.data.table(actions)

# Исправляем орфографическую ошибку в фамилии
actions[Name == 'Ron Wealsey', Name := 'Ron Weasley']

# Сортируем по убыванию колонки tot
actions <- actions[order(-tot)]

# Отбираем первые 15 строк
actions <- actions[1:15]

actions[, names_ordered := gsub('\\s+', '\n', Name)] # переносим имя
actions[, names_ordered := factor(names_ordered, levels = names_ordered)] # сортируем по имени

# Рисуем диаграмму
ggplot(data = actions,
       mapping = aes(x = names_ordered, y = tot)) +
  geom_bar(stat = "identity",
           fill = "grey90",
           color = 'grey85') +
  geom_text(data = actions[Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley')],
            aes(label = tot),
            color = 'darkred',
            vjust = 1.5) +
  geom_text(data = actions[creature == 1],
            aes(label = tot),
            color = 'darkblue',
            vjust = 1.5) +
  geom_text(data = actions[!Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley') &
                             creature == 0],
            aes(label = tot),
            color = 'grey30',
            vjust = 1.5) +
  labs(
    title = 'Character aggression\nHarry Potter universe',
    x = '',
    y = '',
    caption = 'pmsar2020_hw1_Petrenko: barchart'
  ) +
  theme_classic() +
  theme(
    plot.title = element_text(face = 'italic', size = 12, hjust = 0.5),
    plot.caption = element_text(
      colour = 'grey60',
      face = 'italic',
      vjust = 0.2
    )
  )
# Задание 3 ----------------------------------------------------------------

# Импортируем датасет
data <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv'
  )

# Создаем новую переменную 'year'
data[, year := year(dt)]

# Создаем новый датасет, в котором считаем среднюю температуру в году с учетом пропусков
data_temp <-
  data[, list(temp_mn = mean(LandAverageTemperature, na.rm = TRUE)), by = year]


# Делаем график
ggplot(data = data_temp, mapping = aes(x = year, y = temp_mn)) +
  geom_line() +
  geom_smooth(
    method = 'glm',
    color = 'darkred',
    fill = 'darkred',
    alpha = 0.1
  ) +
  geom_smooth(
    method = 'loess',
    color = 'darkblue',
    fill = 'darkblue',
    alpha = 0.1
  ) +
  scale_y_continuous(breaks = c(0.0, 2.5, 5.0, 7.5, 10.0)) +
  ylim(0.0, 10.0)+
  labs(
    title = 'Climat change dynamcs, 1750-2015',
    xaxis = '',
    yaxis = '',
    caption  = 'pmsar2020_hw1_Petrenko: barchart'
  ) +
  theme_classic() +
  theme(
    plot.title = element_text(hjust = 0.5),
    plot.caption = element_text(
      colour = 'grey60',
      face = 'italic',
      hjust = 1
    )
  )
