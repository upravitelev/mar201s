library(data.table)
library(ggplot2)
library(plotly)

# Задание 1---------------------------------------------------------------------

# Импортируем датасет по персонажам Игры Престолов:
got_chars <- fread('https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv')

# Чистка данных:
# Меняем пустое название Дома:
got_chars[house == '', house := 'Unknown']
# Удаляем строки с отрицательным возрастом:
got_chars <- got_chars[age >= 0 | is.na(got_chars$age)]

# Статистика по количеству людей в клане, а также по количеству мужчин и женщин:
got_chars_bar <- got_chars[,list (tot = .N, male = sum(male), female = .N - sum(male)),
                           by = house]
got_chars_bar
# Сортируем датасет по убыванию по количеству членов в клане:
got_chars_bar <- got_chars_bar[order(-tot)]
# Отбираем 10 первых наблюдений: 
got_chars_bar <- got_chars_bar[1:10, ]
# Делаем название клана в две строки:
got_chars_bar <- got_chars_bar[, house := gsub('\\s+', '<br>', house)]
# Задаем сортировку домов:
got_chars_bar <- got_chars_bar[, house := factor(house, levels = house)]

# Рисуем диаграмму в plotly:
# Сначала добавляем ховер с информацией о названии клана, количестве членов в клане, 
#количестве мужчин и количестве женщин в клане: 
got_chars_bar[, my_hover := paste('total members =', tot, '<br>','males =', male, '<br>',
                                  'females =', female)]

plot_ly(got_chars_bar, x = ~house, y = ~tot, type = 'bar', text = ~my_hover, 
        hoverinfo = 'text')%>%
  layout(title = 'Численность Домов в GoT',
         xaxis = list(title = ''),
         yaxis = list(title = ''))

# Задание 2---------------------------------------------------------------------

# Импортируем датасет и называем его actions (использую file.choose, так как, несмотря 
#на наличие файлов в одной папке, периодически не происходит никакого чуда...), 
#нам нужен лист DataSet:
actions <- read_xlsx(file.choose(), sheet = 'DataSet')
# Импорт производим в виде таблицы:
actions <- as.data.table(actions)
# Исправляем ошибку в фамилии Рона:
actions[Name == 'Ron Wealsey', Name := 'Ron Weasley']
# Сортируем  по убыванию колонки tot:
actions <- actions[order(-tot)]
# Отбираем первые 15 строк наблюдений:
actions <- actions[1:15]
# Как и в первом задании, делаем перенос строки (имени персонажа), а затем задаем
#сортировку имен
actions[, names_ordered := gsub('\\s+', '\n', Name)]
actions[, names_ordered := factor(names_ordered, levels = names_ordered)]

# И, наконец, воспроизводим график по образцу и делаем свою подпись:
ggplot(data = actions, mapping = aes(x = names_ordered, y = tot)) +
  geom_bar(stat = "identity",
           fill = "grey90",
           color = 'grey85') +
  geom_text(data = actions[Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley')],
            aes(label = tot),
            color = 'darkred',
            vjust = 1.5) +
  geom_text(data = actions[creature == 1],
            aes(label = tot),
            color = 'darkblue',
            vjust = 1.5) +
  geom_text(data = actions[!Name %in% c('Harry', 'Hermione Granger', 'Ron Weasley') &
                             creature == 0],
            aes(label = tot),
            color = 'grey30',
            vjust = 1.5) +
  labs(title = 'Character aggression\nHarry Potter Universe',
       x = '',
       y = '',
       caption = 'pmsar2020_hw1_svarchevskaya: barchart') +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5),
        plot.caption = element_text(face = 'italic', color = 'grey60', hjust = 1))
#примечание: пришлось гуглить, как у графика сделать не "небо в клеточку", а белый фон
# (хотя раньше уже был опыт, все равно пустилась в поиски)

# Задание 3---------------------------------------------------------------------
# Импортируем файл:
dataset <- fread('https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv')
# Создаем из даты dt переменную года:
dataset[,year := year(dt)]
#примечание: это произошло не с первого раза, так как изначально файл 
#был импортирован по аналогии с заданием 2 (то есть сохранен на компьютер). Видимо,
#макбук решил сыграть злую шутку и при сохранении что-то пошло не так - функция year() упорно не работала :(
#Но при импорте с сайта все пошло как по маслу !

# Теперь уже можно посчитать среднюю температуру в год, при учете пропусков:
dataset <- dataset[, list(temp_mn = mean (LandAverageTemperature, na.rm = TRUE)), by = year]

# Попробуем воспроизвести график среднегодовой температуры:
ggplot(data = dataset, 
       mapping = aes(x = year, y = temp_mn)) +
  geom_line()+
  geom_smooth(method = 'glm', color = 'darkred', fill = 'darkred', alpha = 0.1) + 
  geom_smooth(method = 'loess', color = 'darkblue', fill = 'darkblue', alpha = 0.1) +
  scale_y_continuous(limits = c(0, 10.0), breaks = c(seq(0.0, 10.0, 2.5))) +
  labs(title= "Climate change dynamics, 1750 - 2015",
       caption  = "pmsar2020_hw1_svarchevskaya: line+smooth",
       xaxis = list(title = ''),
       yaxis = list(title = '')) +
  labs ( x = "" ,y="") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5),
        plot.caption = element_text(face = 'italic', color = 'grey60', hjust = 1))

#примечание: проблемы возникли с ценой деления оси ординат: график "размазывался" 
#по всей всей плоскости, а подписаны были только 7.5 и 10.0. 
#Как только к breaks были добавлены limits, проблема была устранена.


