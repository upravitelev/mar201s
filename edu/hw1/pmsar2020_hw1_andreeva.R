# Подготовка к работе -----------------------------------------------------

library(data.table)
library(ggplot2)
library(plotly)
library(readxl)

# Задание 1 ---------------------------------------------------------------

got_chars <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/character-predictions_pose.csv'
  )
class(got_chars) # формат data.table

# Данное выражение означает следующее:
# В базе данных находятся строки, в которых отсутствует название дома (столбец house)
# Важно, что это именно "пустое" значение переменной, а не пропуск (NA)
# Далее отфильтрованным ячейкам присваивается значение Unknown
got_chars[house == '', house := 'Unknown']

# Сохраним номера ненужных наблюдений в отдельный объект
delete <- got_chars[age < 0][[1]]

# Теперь можем от них избавиться, после чего нужно перезаписать объект got_chars
got_chars <- got_chars[-delete]

# Ознакомвшись с таблицей, я поняла, что необходимо поработать со столбцом male
# Для был создан столбец gender со значениями male (если в столбце male находится
# значение 1), и female (если в столбце male находится значение 0)
got_chars[, gender := ifelse(male == 1, 'male', 'female')]

# Посчитаем количество участников для каждого клана/дома
total_members <- got_chars[, .N, by = 'house']

# Посчитаем количество мужчин и женщин для каждого клана/дома
n_gender <- got_chars[, .N, by = c('gender', 'house')]

# В таком виде табличка нам не подойдёт
# Преобразуем её, выделив отдельно столбцы с женщинами и мужчинами 
total_gender <- dcast(n_gender, house ~ gender, value.var = 'N')

# Теперь объединим итоговые таблицы по ключевому столбцу house
got_chars_bar <- merge(total_members, total_gender, by = 'house')

# Сортировка по убыванию количества участников клана
# Выбираем функцию order, тк она возвращает позиции элементов
got_chars_bar <- got_chars_bar[order(N, decreasing = TRUE)]

# Делаем название клана в две строки
got_chars_bar[, house := gsub('\\s+', '<br>', house)]

# Задаем сортировку домов
got_chars_bar[, house := factor(house, levels = house)]

# Создаём необходимый hover
got_chars_bar[, hover := paste(
  'house =',
  house,
  '<br>',
  'total members =',
  N,
  '<br>',
  'males = ',
  male,
  '<br>',
  'females =',
  female
)]

# Строим интерактивный bar_chart с помощью пакета plotly
plot_ly(
  got_chars_bar[1:10],
  x = ~ house,
  y = ~ N,
  type = 'bar',
  text = ~hover,
  hoverinfo = 'text'
) %>%
  layout(
    title = 'Численность Домов в GoT',
    xaxis = list(title = ''),
    yaxis = list(title = '')
  )

# Задание 2 ---------------------------------------------------------------

actions <- read_xlsx('aggressive_actions.xlsx', sheet = 'DataSet')
class(actions) # формат tibble
setDT(actions) # теперь нужный формат data.table

#Исправляем орфографическую ошибку
actions[Name == 'Ron Wealsey', Name := 'Ron Weasley']

#Сортируем по убыванию колонки tot
actions <- actions[order(tot, decreasing = TRUE)]

#Сохраняем первые 15 наблюдений
actions <- actions[1:15]

# Оформляем имена в две строки
actions[, names_ordered := gsub('\\s+', '\n', Name)]

# Задаем сортировку имён
actions[, names_ordered := factor(names_ordered, levels = names_ordered)]

# Устанавливаем основную тему
theme_set(theme_classic())

# Сохраним нужные наблюдения для создания акцентов в отдельные таблички
gold_trio <-
  actions[Name == 'Harry' |
            Name == 'Hermione Granger' | Name == 'Ron Weasley']
creature <- actions[creature == 1]

#Рисуем bar chart
ggplot(data = actions, aes(x = names_ordered, y = tot)) +
  geom_bar(stat = 'identity', color = 'grey85', fill = 'grey90') +
  geom_text(aes(label = tot, vjust = 1.5)) +
  geom_text(data = creature,
            aes(label = tot),
            color = 'darkblue',
            vjust = 1.5) +
  geom_text(data = gold_trio,
            aes(label = tot),
            color = 'darkred',
            vjust = 1.5) +
  labs(
    title = 'Character aggression \n Harry Potter Universe',
    caption = 'pmsar2020_hw1_andreeva: barchart',
    x = NULL,
    y = NULL
  ) +
  theme(
    plot.caption = element_text(colour = 'grey60', face = 'italic'),
    plot.title = element_text(hjust = 0.5),
    legend.position = 'none'
  )

# Задание 3 ---------------------------------------------------------------

my_data <-
  fread(
    'https://gitlab.com/upravitelev/mar201s/-/raw/master/data/global_temperature.csv'
  )

# Создаём переменную года
my_data[, year := year(dt)]  

# Сохраняем средние значения температуры по каждому году
statistics <- my_data[, mean(LandAverageTemperature), by = 'year']

# Переименовываем колонки в соответствии с заданием (и просто для удобства)
setnames(statistics, old = 'V1', new = 'temp_mn')

# Рисуем line chart
ggplot(statistics, aes(x = year, y = temp_mn)) +
  geom_line() +
  geom_smooth(
    method = 'glm',
    formula = y ~ x,
    color = 'darkred',
    fill = 'darkred',
    alpha = 0.1
  ) +
  geom_smooth(
    method = 'loess',
    formula = y ~ x,
    color = 'darkblue',
    fill = 'darkblue',
    alpha = 0.1
  ) +
  lims(y = c(0, 10)) +
  labs(
    title = 'Climat change dynamics, 1750-2015',
    caption = 'pmsar2020_hw1_andreeva: barchart',
    x = NULL,
    y = NULL
  ) +
  theme(
    plot.caption = element_text(colour = 'grey60', face = 'italic'),
    plot.title = element_text(hjust = 0.5)
  )

# Последним дейтсвием в решении домашнего задания стало автоформатирование кода
# Конец :)