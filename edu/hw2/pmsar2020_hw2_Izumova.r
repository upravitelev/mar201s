library(data.table)
library(httr)


### Задание 1


# Напишем цикл, который собирает список характеристик для всех 83 персонажей
sw_chars_table <- lapply(1:83, function(x) {
  api_link <- paste('https://swapi.dev/api/people/', x, sep = '')
  res1 <- GET(api_link)
  res1 <- content(res1)
})

# Проверим класс полученного объекта
class(sw_chars_table)

# Это список, значит нужно преобразовать в дата тэйбл
sw_chars_table <- as.data.table(sw_chars_table)

# Таблица получилась перевёрнутая, поэтому транспонируем её
sw_chars_table <- t(sw_chars_table)
# Это снова список, поэтому вновь преобразуем в дт
sw_chars_table <- as.data.table(sw_chars_table)

# Оставим лишь первые 9 колонок (до homeworld)
sw_chars_table <- sw_chars_table[, 1:9]

# Дадим имена колонкам
setnames(sw_chars_table, new = c("name", "height", "mass", "hair_color", 
                                 "skin_color", "eye_color", "birth_year", 
                                 "gender", "homeworld"))


### Задание 2

# Запишем 9-ю колонку с ссылками на планеты в новую переменную 
planet_nrs <- sw_chars_table[, 9]
# Чтобы функция gsub работала корректно, преобразуем значения из колонки в вектор
planet_nrs <- unlist(planet_nrs)
# С помощью функции gsub удалим из ссылки все элементы, кроме номера планеты 
planet_nrs <- gsub('https://swapi.dev/api/planets/*/','', planet_nrs)
planet_nrs <- gsub('/','', planet_nrs)
# Чтобы номер стал числом, применим as.numeric
planet_nrs <- as.numeric(planet_nrs)

# В основную таблицу добавим колонку number_of_planet - числовой номер домашнего мира героя
sw_chars_table <- sw_chars_table[, number_of_planet := planet_nrs]

# Собираем данные по всем планетам
planets <- lapply(1:60, function(x) {
  api_planet <- paste('https://swapi.dev/api/planets/', x, sep = '')
  planet_chars <- GET(api_planet)
  planet_chars <- content(planet_chars)
})

# Как и выше, преобразуем в таблицу
planets <- as.data.table(planets)
planets <- t(planets)
planets <- as.data.table(planets)
planets <- planets[, 1] # Избавимся от лишних колонок (нужны лишь названия планет)
# Создадим колонку с порядковыми номерами для каждой планеты
planets[, number_of_planet := 1:60]

# Объединим основную таблицу и таблицу с именами планет по ключу number_of_planet
sw_chars_table <- merge(sw_chars_table, planets, by = 'number_of_planet')
# Удаляем лишние колонки, меняем название
sw_chars_table[, number_of_planet := NULL]
sw_chars_table[, homeworld := NULL]
setnames(sw_chars_table, old = 'V1', 'homeworld')


### Задание 3

# Не вышло :(









