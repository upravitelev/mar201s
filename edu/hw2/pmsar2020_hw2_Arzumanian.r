#Домашняя работа №2
#Арзуманян Илона

#Задание 1

#В первую очередь скачиваем и устанавливаем все пакеты, которые понадобятся
#для работы с извлечением информации
install.packages("data.table")
install.packages("httr")
install.packages("jsonlite")
library(data.table)
library(httr)
library(jsonlite)

#Поскольку в рамках задания нас интересует не вся информация с сайта, а только 
#информация по героям, нам понадобятся данные по ссылке http://swapi.dev/api/people/

#Подготавливаем таблицу sw_chars_table для импорта данных
sw_chars_table <- data.table(
  name = character(), 
  gender = character(), 
  hair_color = character(),
  skin_color = character(),
  birth_year = character(),
  homeworld = character()
)

#Выдача дает информацию только по 10 героям, а не 
#по 83, поэтому нам придется сделать запросы с нескольких страниц (т.к. 8 нам не
#хватит, возьмем 9). Для этого используем цикл while с его счетчиком

i <- 1 
while (i < 10) { #проходимся до 10, т.к. 10 не включается
  main_url <- paste('http://swapi.dev/api/people/?page=', i, sep = '') #добавляем к ссылке номер страницы, указываем, что в качестве разделителя ничего нет, чтобы пройтись по ним
  heroes_json <- content(GET(url = main_url)) #кладем сюда все данные
  for (a in 1:10) {  #Для каждого героя надо собрать таблицу с искомыми данными (именем, полом и т.д.)
    heroes_full <- data.table(
      heroes_json$results[[a]]$name,
      heroes_json$results[[a]]$gender,
      heroes_json$results[[a]]$hair_color,
      heroes_json$results[[a]]$skin_color,
      heroes_json$results[[a]]$birth_year,
      heroes_json$results[[a]]$homeworld
    )
    sw_chars_table <- rbind(sw_chars_table, heroes_full, use.names=FALSE)#объединяем все таблицы, чтобы получить общую, единую таблицу
  }
  i <- i + 1 #показываем, что счетчик меняет значения
}


#Задание 2

#Извлекаем как вектор данные по планетам (6 столбец)
v_planet<-as.character(as.vector(sw_chars_table[,6]))

#Уникализируем вектор, убираем повторяющиеся значения
planet_unique <- unique(v_planet,by = 'homeworld')


