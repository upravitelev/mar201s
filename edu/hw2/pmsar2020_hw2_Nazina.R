
# ЗАДАНИЕ 1 ---------------------------------------------------------------

# Найдите способ, как можно с сайта импортировать данные по героям саги.
library('httr')
library('data.table')

# Автоматически собираем информацию при помощи функции lapply (с 1 по 82 ячейку)
# Создаем GET-запрос для извлечения данных о персонажах с сервера
sw_chars <- lapply(1:83, function(x) {
  path <- paste('https://swapi.dev/api/people/', x, sep = '')
  sw_chars <- GET(path)
  sw_chars <- content(sw_chars)
})
# Записываем полученные данные в табличном виде
sw_chars_table <- as.data.table(sw_chars)
# Транспонируем таблицу
sw_chars_table <- t(sw_chars_table)
# Снова записываем в формате data table
sw_chars_table <- as.data.table(sw_chars_table)
# Удаляем ненужные столбцы
sw_chars_table[, V2 := NULL]
sw_chars_table[, V3 := NULL]
sw_chars_table[, V6 := NULL]
sw_chars_table[, V10 := NULL]
sw_chars_table[, V11 := NULL]
sw_chars_table[, V12 := NULL]
sw_chars_table[, V13 := NULL]
sw_chars_table[, V14 := NULL]
sw_chars_table[, V15 := NULL]
sw_chars_table[, V16 := NULL]
sw_chars_table

# Перезаписываем новые имена столбцов
setnames(sw_chars_table,"V1", "name")
setnames(sw_chars_table,"V4", "hair_color")
setnames(sw_chars_table,"V5", "skin_color")
setnames(sw_chars_table,"V7", "birth_year")
setnames(sw_chars_table,"V8", "gender")
setnames(sw_chars_table,"V9", "homeworld")
sw_chars_table


# ЗАДАНИЕ 2 ---------------------------------------------------------------

# Записать в поле homeworld нормальное значение вместо ссылки
# Выбираем столбец homeworld и возвращаем его как вектор
homeworld <- sw_chars_table[, homeworld]
head(homeworld)

sw_planet <- lapply(1:60, function(x) {
  p_path <- paste('http://swapi.dev/api/planets/', x, sep = '')
  sw_planet <- GET(p_path)
  sw_planet <- content(sw_planet)
})
# Записываем полученные данные в табличном виде
sw_planet_table <- as.data.table(sw_planet)
# Транспонируем таблицу
sw_planet_table <- t(sw_planet_table)
# Снова записываем в формате data table
sw_planet_table <- as.data.table(sw_planet_table)

# Удаляем ненужные столбцы
sw_planet_table[, V2 := NULL]
sw_planet_table[, V3 := NULL]
sw_planet_table[, V4 := NULL]
sw_planet_table[, V5 := NULL]
sw_planet_table[, V6 := NULL]
sw_planet_table[, V7 := NULL]
sw_planet_table[, V8 := NULL]
sw_planet_table[, V9 := NULL]
sw_planet_table[, V10 := NULL]
sw_planet_table[, V11 := NULL]
sw_planet_table[, V12 := NULL]
sw_planet_table[, V13 := NULL]
sw_planet_table[, V14 := NULL]
sw_planet_table