# HW2_Filippov ------------------------------------------------------------
library(rvest)
library(httr)
library(data.table)

# Task 1 ------------------------------------------------------------------
# ����� ���� lapply �������� ���������� �� ���� ����������
final_table <- lapply(1:83, function(i) {
  query1 <- paste('https://swapi.dev/api/people/', i, '/', sep = '')
  starwars_json <- GET(url = query1)
  starwars_json <- content(starwars_json)
  fin_dt <- data.table(
    name = starwars_json$name,
    gender = starwars_json$gender,
    hair_color = starwars_json$hair_color,
    skin_color = starwars_json$skin_color,
    birth_year = starwars_json$birth_year,
    hero_homeworld = starwars_json$homeworld
  )
})
# ��������� �� � ���� �������
sw_chars_table <- rbindlist(final_table, fill = TRUE)


# Task 2 ------------------------------------------------------------------
#������� ������� ������ ��� �������, ��� �� �����, ��� �� 60

planet_table <- lapply(1:60, function(i) {
  query2 <- paste('https://swapi.dev/api/planets/', i, '/', sep = '')
  planet_json <- GET(url = query2)
  planet_json <- content(planet_json)
 
  final_planet <- data.table(
    hero_homeworld = query2,
    planet = planet_json$name
  )
})
sw_planets <- rbindlist(planet_table, fill = TRUE)

#��� ������ ������� merge ��������� ��������� ��� �������
sw_chars_table <- merge(x = sw_chars_table, y = sw_planets, by = 'hero_homeworld')

#������� ��� ������� ������� �� �������� �� �������
sw_chars_table[, hero_homeworld := NULL]


# Task 3 ------------------------------------------------------------------
# ������ ������ ������� ��� ������, �.�.����������� ������� ������������� �� �������� ������
sw_search_table <- rbindlist(final_table, fill = TRUE)

# ������� �������, ������� ���� ����� ������ �� ����� ��������� � ��������� ��� � ������ ��� �������
search_hero <- function(x) {
  name_search <- which(sw_search_table$name == x)
  query3 <- paste('https://swapi.dev/api/people/', name_search, '/', sep = '')
  searcher_sw_json <- GET(url = query3)
  searcher_sw_json <- content(searcher_sw_json)
  print(searcher_sw_json)
}

# �� ���� ������� ������ �� ���������� ����������, ���������� ������� ���:
#  name_search <- which(x %in% sw_search_table$name), �� �� ���������
