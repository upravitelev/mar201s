# Нам потребуются две уже установленные ранее библиотеки:
library(httr)
library(data.table)
#Задание 1----------------------------------------------------------------------
# Сначала при помощи lapply() создадим функцию, которая пройдется по всем
##83 (позднее мы увидим, что их 82) персонажам и соберет информацию о них:
people <- lapply(1:83, function(x) {
  people_url <- paste('https://swapi.dev/api/people/', x, '', sep = '')
  people_json <- GET(url = people_url)
  people_json <- content(people_json)
return(people_json)
})
# Итак, мы получили 83 списка, внутри которых вся информация о героях.
##17-й список действительно оказался пустым.
## Примечание: не всегда при первом запуске функции происходит выгрузка 
###в Environment, видимо, это немного "тормозит" система.

# Теперь же при помощи того же lapply() мы можем собрать функцию, которая
##"пробежит" по всем 83 спискам и сделает из них таблички (17-я таблица пустая). 
## Предварительно мы можем посмотреть в списках, какой вид у ссылки, 
##чтобы привести его к универсальному, подставив вместо числа x:
sw <- lapply(1:83, function(x) {
  sw_chars_table <- data.table(
    name = people[[x]][["name"]],
    gender = people[[x]][["gender"]],
    hair_color = people[[x]][["hair_color"]],
    skin_color = people[[x]][["skin_color"]],
    birth_year = people[[x]][["birth_year"]],
    homeworld = people[[x]][["homeworld"]]
  )
return(sw_chars_table)
})

# Собираем 82 таблички в одну:
sw_chars_table <- rbindlist(sw)

#Задание 2----------------------------------------------------------------------
# Используем функцию из прошлого задания и немного видоизменим ее. Теперь мы
##пройдем по 82 персонажам из нашей таблицы, "вытащим" оттуда ссылки на homeworld
##и выгрузим по этим ссылкам информацию по планетам (получится 82 списка):
planet <- lapply(1:82, function(x) {
  planet_url <- sw_chars_table[x, homeworld]
  planet_json <- GET(url = planet_url)
  planet_json <- content(planet_json)
return(planet_json)
})

# Достанем из этих списков только название планеты (по аналогии с первым 
##заданием) и сделаем 82 таблички с названиями планет:
planet <- lapply(1:82, function(x) {
  new_table <- data.table(planet = planet[[x]][["name"]])
return(new_table)
})

# Соединяем 82 таблички в одну, получается единый столбец:
plnt <- rbindlist(planet)

# Добавляем этот столбец к нашей первоначальной таблице:
sw_chars_table2 <- data.table(sw_chars_table, plnt)

#Задание 3----------------------------------------------------------------------
# Собираем функцию, которая будет выгружать информацию по имени героя из 
##ранее созданной нами таблицы (возьмем вторую с названиями планет):
search_hero <- function(x) {
  hero_inform <- sw_chars_table2[name == x]
  return(hero_inform)
}
search_hero('Obi-Wan Kenobi')

#Задание 3 (попытки расширенной версии)-----------------------------------------
# Если нам нужна вся информация по персонажам, можно повторить сделанное
##в первом задании, только усовершенствовать таблицу:
full_sw <- lapply(1:83, function(x) {
  full_sw_chars_table <- data.table(
    name = people[[x]][["name"]],
    gender = people[[x]][["gender"]],
    hair_color = people[[x]][["hair_color"]],
    skin_color = people[[x]][["skin_color"]],
    birth_year = people[[x]][["birth_year"]],
    homeworld = people[[x]][["homeworld"]],
    height = people[[x]][["height"]],
    mass = people[[x]][["mass"]],
    eye_color = people[[x]][["eye_color"]],
    films = people[[x]][["films"]],
    species = people[[x]][["species"]],
    vehicles = people[[x]][["vehicles"]],
    starships = people[[x]][["starships"]],
    created = people[[x]][["created"]],
    edited = people[[x]][["edited"]],
    url = people[[x]][["url"]]
  )
return(full_sw_chars_table)
})

# Соединяем в новую таблицу:
full_sw_chars_table <- rbindlist(full_sw)
# Небольшая проблемка: имя персонажа дублируется из-за транспорта, кораблей и 
##фильмов :(

# Запускаем аналогичную функцию поиска по имени:
search_hero2 <- function(x) {
  hero_infor <- full_sw_chars_table[name == x]
  return(hero_infor)
}
search_hero2('Obi-Wan Kenobi')
# Как следствие, в выведенных табличках появляются дубли информации 
##(зато ничего не упущено...)
# Решила задачу таким образом, так как не получилось достать из изначального списка :(
##URLencode(), соответственно, не потребовался.




