library('rvest')
library('httr')
library('data.table')

#задание 1
#импорт данных по героям саги
#указываем resource url 
api_heroes <- 'https://swapi.dev/api/people/'
#используем функцию GET
heroes <-GET(api_heroes)
#функция content возвращает объект в виде сложного списка с вложениями
heroes <-content(heroes)
str(heroes)

# запускаем lapply-выражение
sw_chars <-lapply(1:83, function(x) {
  path <- paste('https://swapi.dev/api/people/', x, sep = ' ') #собираем все в один url-запрос
  sw_chars <- GET(path) #запрос
  sw_chars <- content(sw_chars)
})

#создаем таблицу с релевантной инфорамцией
sw_chars_table <- as.data.table(sw_chars)
#транспонируем таблицу
sw_chars_table <- t(sw_chars_table)
#и снова запускаем
sw_chars_table <- as.data.table(sw_chars_table)
#переименовываем названия столбцов
sw_chars_table <- sw_chars_table[ , list(name = V1, gender = V8, hair_color = V4, skin_color= V5, birth_year= V7, homeworld= V9)]
sw_chars_table <- sw_chars_table[, .(name, gender, hair_color, skin_color, birth_year, homeworld)]

#задание 2
#Заменить в таблице sw_chars_table значения в поле homeworld
#выводим столбец с homeworld
planets <- sw_chars_table[, 6] 
#применяем функцию unlist, чтобы преобразованть список планет в вектор 
planets <- unlist(planets) 
#убираем из ссылки все элементы кроме цифр
planets <- gsub('\\D+','', planets) 
#преобразовываем в числовое значение
planets <- as.numeric(planets) 
#добавляем в таблицу столбец planet_number
sw_chars_table <- sw_chars_table[, planet_number := planets] 

#запускаем lapply-выражение и извлекаем ифнормацию
planet <- lapply(1:60, function(x) { 
  path1 <- paste('https://swapi.dev/api/planets/', x, sep = '') #собираем все в один url-запрос
  planet_chars <- GET(path1) #запрос
  planet_chars <- content(planet_chars) 
}) 

#создаем таблицу с релевантной инфорамцией
planet <- as.data.table(planet) 
#транспонируем таблицу
planet <- t(planet) 
#запускаем
planet <- as.data.table(planet) 
#выводим колонку с названиями планет
planet <- planet[, 1] 
planet[, planet_number := 1:60] 
#соединяем 2 предыдущие таблицы
sw_chars_table <- merge(sw_chars_table, planet, by = 'planet_number') 

sw_chars_table[, planet_number := NULL] 
sw_chars_table[, homeworld := NULL]


#задание 3
#запускаем функцию, которая возвращает информации по герою
search <- function (name) { 
name_encode <- URLencode(name) #используем URLencode(), чтобы обработать пробел в имени героя 
  path2 <- paste('https://swapi.dev/api/people/?search=', name_encode, sep = '') #собираем все в один url-запрос
  hero_name <- GET(path2) #запрос
  hero_content <- content( hero_name)$results #обращаемся к results, т.к. нам нужна информация отттуда
  return(hero_content) 
} 

# делаем проверку
search_hero <- search('Luke Skywalker') 
str(search_hero)



