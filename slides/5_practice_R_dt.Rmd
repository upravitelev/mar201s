---
title: "data.table pt2"
author: "Philipp A.Upravitelev <br> <upravitelev@gmail.com> <br>"
date: '`r Sys.Date()`'
output:
  ioslides_presentation:
    autosize: yes
    logo: ./pics/Rlogo.svg
    transition: slower
    widescreen: yes
  beamer_presentation: default
---

# data.table intro

## установка и подключение

```{r}
# install.packages('data.table')
library(data.table)
```


## создание таблицы

```{r}
dt1 <- data.table(
  month_names = month.name,
  month_abb = month.abb,
  month_ord = seq_len(length(month.abb)),
  is_winter = grepl('Jan|Dec|Feb', month.abb)
)
```

----

```{r}
dt1
```

## общая форма data.table
![](https://gitlab.com/upravitelev/r_webinars/-/raw/2f5b2bfb46328d0c9618364929e4f191aa9b4c7b/slides/pics/dt_syntax.png)


## фильтрация строк {.build}

По номеру строки:
```{r}
dt1[1, ]
```

По условию:
```{r}
dt1[month_ord <= 3]
```

## выбор колонки {.build}

Выбор значений колонки:
```{r dt2}
dt1[, month_names]
```

Выделение колонки в новую таблицу:
```{r}
dt1[, list(month_names)]
```

## выбор нескольких колонок {.build}
```{r dt3}
dt1[, list(month_names, month_abb)]
```

## создание колонок {.build}
```{r dt12}
dt1[, new_col := 12:1]
dt1
```

## модификация колонок {.build}
```{r}
dt1[month_ord <= 5, new_col := NA]
dt1
```

# Применение функций

## dataset {.build}

```{r}
# импортируем по ссылке
sw <- fread('http://bit.ly/39aOUne')

# смотрим структуру объекта
str(sw)
```


## простое применение функций {.build}

```{r}
sw[, median(mass, na.rm = TRUE)]
```

## функция + list() {.build}

```{r}
sw[, list(median(mass, na.rm = TRUE))]
```

```{r}
sw[, list(mass_md = median(mass, na.rm = TRUE))]
```

## Количество строк и значений: .N, uniqueN {.build}

```{r}
sw[, .N]
```

```{r}
sw[, uniqueN(planet_name)]
```

## вхождение строки: grep / grepl {.build}

```{r}
my_vec <- c('ab', 'bc', 'cd', 'be', 'ac')
grep('b', my_vec)
```

```{r}
grepl('b', my_vec)
```

----

```{r}
sw[grep('grey', skin_color)]
```


## ветвления: ifelse / fifelse {.build}

```{r}
ifelse(5 == 3, 'значение если верно', 'значение если неверно')
```

## {.build}

```{r}
sw[, new := fifelse(grepl('grey', skin_color), 'grey', 'no grey')]
sw[, list(name, skin_color, new)]
```


# Агрегации

## split-apply-combine

<!-- <img src="./pics/merge.jpg" width="80%" style="position:absolute;left:5%;"> -->
![](./pics/split-apply-combine.jpg)


## вычисления по одной колонке {.build}

```{r}
sw[, uniqueN(name), by = gender]
```

```{r}
sw[, list(n_chars = uniqueN(name)), by = gender]
```

## вычисления по нескольким колонкам {.build}

```{r}
sw[, list(
  n_chars = uniqueN(name),
  mass_md = median(mass, na.rm = TRUE)
), by = gender]
```

## группировка по нескольким полям {.build}
```{r}
sw_grps <- sw[gender %in% c('male', 'female'),
              list(n_chars = uniqueN(name)),
              by = list(gender, skin_color)]
sw_grps[1:5]
```

## вычисления с группирующей колонкой {.build}
```{r}
sw_skin <- sw[gender %in% c('male', 'female'),
              list(n_chars = uniqueN(name)),
              by = list(gender,
                        skin_color_group = ifelse(grepl(',', skin_color), 'multi', 'mono'))]
sw_skin
```

## вычисления в группах на месте {.build}

```{r}
sw_skin[, total_chars := sum(n_chars), by = skin_color_group]
sw_skin
```

<!-- # Фильтрация колонок -->

<!-- ## colname[x] {.build} -->
<!-- ```{r} -->
<!-- sw[, list( -->
<!--   male_mass = mean(mass[gender == 'male'], na.rm = TRUE), -->
<!--   male_mass_from_tt = mean(mass[gender == 'male' & planet_name == 'Tatooine'], na.rm = TRUE) -->
<!-- )] -->
<!-- ``` -->

<!-- # Операции с таблицами -->

<!-- ## rbind() -->
<!-- ```{r dt6} -->
<!-- # создаем первую таблицу -->
<!-- dt1 <- data.table(tb = 'table_1', -->
<!--                   col1 = sample(9, 3), -->
<!--                   col3 = 'only in table1', -->
<!--                   col2 = sample(letters, 3)) -->

<!-- # создаем вторую таблицу -->
<!-- dt2 <- data.table(tb = 'table_2', -->
<!--                   col4 = 'only in table2', -->
<!--                   col1 = sample(9, 3), -->
<!--                   col2 = sample(letters, 3)) -->
<!-- ``` -->

<!-- ## {.build} -->
<!-- ```{r dt7, eval=FALSE} -->
<!-- # объединяем по строкам -->
<!-- rbind(dt1, dt2, use.names = TRUE, fill = TRUE) -->
<!-- ``` -->

<!-- ```{r, echo=FALSE} -->
<!-- rbind(dt1, dt2, use.names = TRUE, fill = TRUE) -->
<!-- ``` -->


<!-- ## cbind() {.build} -->

<!-- ```{r dt9, eval=FALSE} -->
<!-- cbind(dt1, dt2) -->
<!-- ``` -->

<!-- ```{r, echo=FALSE} -->
<!-- cbind(dt1, dt2) -->
<!-- ``` -->


<!-- ## merge() -->
<!-- ```{r dt11} -->
<!-- # создаем датасет 1, в синтасисе data.table -->
<!-- dt1 <- data.table(key_col = c('r1', 'r2', 'r3'), -->
<!--                   col_num = seq_len(3)) -->

<!-- # создаем датасет 2, в синтасисе data.table -->
<!-- dt2 <- data.table(key_col = c('r3', 'r1', 'r2'), -->
<!--                   col_char = c('c', 'a', 'b')) -->
<!-- ``` -->

<!-- ---- -->

<!-- ```{r} -->
<!-- # сливаем построчно по значениям в колонке key_col -->
<!-- merge(x = dt1, y = dt2, by = 'key_col', all = FALSE) -->
<!-- ``` -->


<!-- ------- -->

<!-- <img src="./pics/merge.jpg" width="80%" style="position:absolute;left:5%;"> -->


<!-- ## dcast -->

<!-- ```{r} -->
<!-- dt_long <- data.table( -->
<!--   id = paste0('id_', rep(1:2, each = 2)), -->
<!--   variable = rep(c('age', 'height'), 2), -->
<!--   value = c(25, 175, 31, 176) -->
<!-- ) -->
<!-- dt_long -->
<!-- ``` -->

<!-- ---- -->

<!-- ```{r} -->
<!-- dt_wide <- dcast(data = dt_long, formula = id ~ variable, value.var = 'value') -->
<!-- dt_wide -->
<!-- ``` -->

<!-- ## -->

<!-- ```{r, echo=FALSE} -->
<!-- dt_long <- data.table( -->
<!--   # две волны, по два респондента в каждой -->
<!--   wave = paste0('wave_', rep(1:2, each = 6)), -->
<!--   # на каждого респондента задаем три строки -->
<!--   id = paste0('id_', rep(1:4, each = 3)), -->
<!--   # три характеристики повторяем для четырех респондентов -->
<!--   variable = rep(c('age', 'height', 'weight'), 4), -->
<!--   # задаем значения характеристик, с учетом того, как упорядочены первые две колонки -->
<!--   value = c(45, 163, 55, -->
<!--             13, 142, 40, -->
<!--             29, 178, 85, -->
<!--             69, 155, 63)) -->
<!-- dt_long -->
<!-- ``` -->


<!-- ---- -->

<!-- ```{r} -->
<!-- dt_wide <- dcast(data = dt_long, formula = wave + id ~ variable, value.var = 'value') -->
<!-- dt_wide -->
<!-- ``` -->

<!-- ---- -->

<!-- ```{r} -->
<!-- dt_wide <- dcast(data = dt_long, formula = wave ~ variable, -->
<!--                  value.var = 'value', fun.aggregate = mean) -->
<!-- dt_wide -->
<!-- ``` -->


<!-- ## melt -->

<!-- ```{r} -->
<!-- dt_wide <- dcast(data = dt_long, formula = wave + id ~ variable, value.var = 'value') -->
<!-- dt_wide -->
<!-- ``` -->

<!-- ## {.build} -->

<!-- ```{r, eval = FALSE} -->
<!-- melt(data = dt_wide, -->
<!--      id.vars = c('wave', 'id'), measure.vars = c('age', 'height', 'weight'), -->
<!--      variable.name = 'variable', value.name = 'value') -->
<!-- ``` -->

<!-- ```{r, echo=FALSE} -->
<!-- melt(data = dt_wide, -->
<!--      id.vars = c('wave', 'id'), measure.vars = c('age', 'height', 'weight'), -->
<!--      variable.name = 'variable', value.name = 'value') -->
<!-- ``` -->


-------

<div style="position:absolute;bottom:10%;right:6%;">
![](./pics/tzeentch2.png)
</div>

