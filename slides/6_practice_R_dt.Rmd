---
title: "data.table pt3"
author: "Philipp A.Upravitelev <br> <upravitelev@gmail.com> <br>"
date: '`r Sys.Date()`'
output:
  ioslides_presentation:
    autosize: yes
    logo: ./pics/Rlogo.svg
    transition: slower
    widescreen: yes
  beamer_presentation: default
---


# Контрольная работа

## 

Создайте вектор `vec`, состоящий из элементов 13, 950, 209, 918, 658, 185, 988, 410, 961, 908.	

Извлеките из вектора `vec` и выведите на печать третье, четвертое и седьмое значения.	

**В векторе `vec` замените три случайных значения на NA**

Создайте вектор `vec` из трех элементов со значениями: 5, FALSE, 56. Выведите на печать. Проверьте тип объекта. Объясните результат.	

**Добавьте к вектору из предыдущего задания еще один элемент, 21. Выведите результат на печать. Добавьте еще последовательность от 17 до 43 с шагом 7. Выведите результат на печать.**

## 
Посчитайте, сколько чисел, делящихся нацело на 7, в ряду от 1 до 100.	

Создайте вектор с набором чисел от 10 до 30. Выберите и напечатайте все числа из вектора, которые больше 15 и меньше 25.	

Из вектора `vec <- sample(100, 8)` выберите максимальное и минимальное значения.	

**Выберите и выведите на печать все значения вектора `x <- c(1, 3, 5, 6)`, которые встречаются в векторе `y <- sample(10, 8)`.**

Объясните, что происходит в выражении vec_full <- sort(sample(vec_full, 25, replace = TRUE), decreasing = TRUE). Вектора vec_full у вас нет, только код выражения.

# Домашняя работа

# Манипуляции с таблицами

## merge()
```{r dt11}
library(data.table)
# создаем датасет 1, в синтасисе data.table
dt1 <- data.table(key_col = c('r1', 'r2', 'r3'),
                  col_num = seq_len(3))

# создаем датасет 2, в синтасисе data.table
dt2 <- data.table(key_col = c('r3', 'r1', 'r2'),
                  col_char = c('c', 'a', 'b'))
```

----

```{r}
# сливаем построчно по значениям в колонке key_col
merge(x = dt1, y = dt2, by = 'key_col', all = FALSE)
```


-------

<img src="./pics/merge.jpg" width="80%" style="position:absolute;left:5%;">


## dcast

```{r}
dt_long <- data.table(
  id = paste0('id_', rep(1:2, each = 2)),
  variable = rep(c('age', 'height'), 2),
  value = c(25, 175, 31, 176)
)
dt_long
```

----

```{r}
dt_wide <- dcast(data = dt_long, formula = id ~ variable, value.var = 'value')
dt_wide
```

## 

```{r, echo=FALSE}
dt_long <- data.table(
  # две волны, по два респондента в каждой
  wave = paste0('wave_', rep(1:2, each = 6)),
  # на каждого респондента задаем три строки
  id = paste0('id_', rep(1:4, each = 3)),
  # три характеристики повторяем для четырех респондентов
  variable = rep(c('age', 'height', 'weight'), 4),
  # задаем значения характеристик, с учетом того, как упорядочены первые две колонки
  value = c(45, 163, 55,
            13, 142, 40,
            29, 178, 85,
            69, 155, 63))
dt_long
```


---- 

```{r}
dt_wide <- dcast(data = dt_long, formula = wave + id ~ variable, value.var = 'value')
dt_wide
```

---- 

```{r}
dt_wide <- dcast(data = dt_long, formula = wave ~ variable, 
                 value.var = 'value', fun.aggregate = mean)
dt_wide
```


## melt

```{r}
dt_wide <- dcast(data = dt_long, formula = wave + id ~ variable, value.var = 'value')
dt_wide
```

## {.build}

```{r, eval = FALSE}
melt(data = dt_wide, 
     id.vars = c('wave', 'id'), measure.vars = c('age', 'height', 'weight'),
     variable.name = 'variable', value.name = 'value')
```

```{r, echo=FALSE}
melt(data = dt_wide, 
     id.vars = c('wave', 'id'), measure.vars = c('age', 'height', 'weight'),
     variable.name = 'variable', value.name = 'value')
```

<!-- # data.table tips -->

<!-- ## .N {.build} -->
<!-- ```{r} -->
<!-- payments[, .N] -->
<!-- ``` -->


<!-- ```{r} -->
<!-- users[, .N, by = media_source] -->
<!-- ``` -->

<!-- ## column[x] -->

<!-- ```{r} -->
<!-- total <- merge( -->
<!--   users, -->
<!--   payments[, list(gross = sum(gross), purchases = .N), by = user_id], -->
<!--   by = 'user_id', -->
<!--   all.x = TRUE -->
<!-- ) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- total[1:7] -->
<!-- ``` -->

<!-- ---- -->

<!-- ```{r, eval=FALSE} -->
<!-- total[, list(total_users = length(unique(user_id)), -->
<!--              payers = length(unique(user_id[!is.na(gross)]))),  -->
<!--       by = media_source] -->
<!-- ``` -->

<!-- ```{r, echo=FALSE} -->
<!-- total[, list(total_users = length(unique(user_id)), -->
<!--              payers = length(unique(user_id[!is.na(gross)]))),  -->
<!--       by = media_source] -->
<!-- ``` -->

<!-- ## uniqueN() -->

<!-- ```{r} -->
<!-- total[, list(total_users = uniqueN(user_id), -->
<!--              payers = uniqueN(user_id[!is.na(gross)])),  -->
<!--       by = media_source] -->
<!-- ``` -->

-------

<div style="position:absolute;bottom:10%;right:6%;">
![](./pics/tzeentch2.png)
</div>
