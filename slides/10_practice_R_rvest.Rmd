---
title: "rvest package"
author: "Philipp A.Upravitelev <br> <upravitelev@gmail.com> <br>"
date: '`r Sys.Date()`'
output: 
  ioslides_presentation:
      autosize: true
      transition: slower
      widescreen: true
editor_options: 
  chunk_output_type: console
---
```{r load-packages, include = FALSE, message=FALSE, warning=FALSE}
pkgs <- c("rmarkdown", "knitr", "data.table", "ggplot2", "rvest")
for (pkg in pkgs) 
  require(pkg, character.only = TRUE, quietly = TRUE, warn.conflicts = FALSE)
```

# Internet

## intro

 - 1967 - ARPANet
 - 1971 - email
 - 1984 - Domain Name System
 - 1989 - HTTP, HTML
 - 1990 - dial-up
 - 2001 - Dot-com bubble
 - 2005 - web 2.0
 - 2019 - 53.6% users worldwide


## DNS servers

![](./pics/rvest_dns_servers.png){width=60%;position=left}

# HTML

## page structure
```{r, eval = FALSE}
<!DOCTYPE HTML>
<html>
  <head>
    <title></title>
  </head>
  <body>
    <h1></h1>
    <p></p>
  </body>
</html>
```

## page example
```{js, eval = FALSE}
<!DOCTYPE HTML>
<html>
  <body>
    <h2>Правда о лосях</h2>
    <ol>
      <li>Лось — животное хитрое</li>
      <li>...и коварное!</li>
    </ol>
  </body>
</html>
```

##

  <body>
    <h2>Правда о лосях в html-тегах</h2>

    <h3>это пример параграфа</h3>
    <p style="color:red;">
      Здесь мы расскажем вам всю правду, какую знаем о лосях
    </p>

    <h3>это пример списка</h3>
    <ol>
      <li>Лось — животное хитрое</li>
      <li>...и коварное!</li>
    </ol>

    <h3>это пример тега с классом</h3>
    <p class='my_class'>дальше будет дичь</p>
  </body>


## close your tags

![](./pics/rvest_tags.jpg){width=60%;position=left}

 
 
# rvest

## install package

```{r}
# install.packages('rvest')
library(rvest)
```

<!-- ## импорт страницы -->

```{r}
url <- read_html('../data/elk.html', encoding = 'UTF-8')
```

## Выбор узлов {.build}
```{r}
html_node(url, xpath = '//h2')
```

```{r}
html_node(url, xpath = '//h2') %>%
  html_text()
```


## Навигация по дереву тегов

- `%tag_name%`:	выбирает все теги с названием `%tag_name%`

- `/`: Иcпользуется для указания прямого пути к элементу от корневого/родительского тега. Если весь xpath-путь начинается с `/`, то это абсолютный путь от корня дерева

- `//`: Выбирает теги независимо от места их расположения, при этом может использоваться как в начале пути, так и в середине

- `@`: выбор атрибута узла

- `*`: знак подстановки, "на этом месте может быть что угодно"

## применение `/` и `//` {.build}

```{r}
html_node(url, xpath = '/html/body/h2') %>%
  html_text()
```

```{r}
html_node(url, xpath = '//h2') %>%
  html_text()
```

## предикаты {.build}

`[x]` - идентификаторы номера тега в списке одинаковых тегов одной иерархии

```{r}
html_node(url, xpath = '//li[2]') %>%
  html_text()
```

## применение `@` {.build}

```{r}
html_nodes(url, xpath = '//p[2]') %>%
  html_text()
```

```{r}
html_node(url, xpath = '//p[@class="my_class"]') %>%
  html_text()
```


