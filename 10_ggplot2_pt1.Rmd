## Запись занятий 

<iframe width="560" height="315" src="https://www.youtube.com/embed/sfOEzDVCiBk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

Все записи организованы в [плейлист](https://www.youtube.com/playlist?list=PLEwK9wdS5g0rzlOC2O3Oa4nlHpU4ai-FK)

<br>

## ggplot2 package

### Компоненты графика

Все графики, которые можно создать с помощью пакета, могут быть разделены на несколько компонентов:

- данные
- пространство координат
- визуальные параметры (aesthetics)
- геометрическая форма представления данных (geom)
- статистические вычисления и трансформации (stats)
- параметры смещения графиков на координатной сетке (position adjustment)
- параметры осей (scales)
- фасеты (группировка, facets)

Ниже представлен типовой код создания графика с помощью пакета `ggplot2`. На графике отражены все перечисленные структурные элементы: данные (объект `mpg`), координатная сетка (`coord_cartesian()`), параметры визуализаций (функция `aes()` и ее аргументы). Основная особенность синтаксиса `ggplot2` - его аддитивность, когда объект графика создается путем постепенного добавления к базовому объекту новых элементов и/или параметров визуализаций. Базовый объект создается с помощью функции `ggplot()`, а различные слои, геометрические объекты или текстовые метки, параметры визуализаций и т.д. - другими функциями, через внутренний оператор `+`:

```{r, eval = FALSE}
ggplot() +
  geom_smooth(method = 'lm') +
  geom_point(data = mpg, mapping = aes(x = hwy, y = cty, color = cyl)) +
  coord_cartesian() +
  scale_color_gradient() +
  theme_classic()
```

<br>

### Базовые геомы

#### geom_point()
Функция `geom_point()` позволяет конструировать точечные диаграммы (графики рассеяния). Стандартные аргументы функций: `data` (таблица, данные из которой надо отрисовать),  `mapping` - с помощью этого аргумента указываются, какие колонки таблица как именно будут использованы при отрисовке (какие будут задавать оси, какие - цвета или форму).

```{r}
# подключаем библиотеки
library(ggplot2)
library(data.table)

# импорт данных
imdb_link <- 'https://gitlab.com/upravitelev/mar201s/raw/master/data/IMDb movies.csv'
tg_cols <- c("director", "title", "original_title", "year", 
             "genre", "duration", "country", "avg_vote")

imdb <- fread(imdb_link, select = tg_cols)
imdb[, year := as.numeric(year)]

imdb_lynch <- imdb[director == 'Woody Allen']
imdb_martin <- imdb[director == 'Martin Scorsese']
imdb_lynch <- imdb[director == 'David Lynch']

imdb_genres <- imdb[genre %in% c('Horror', 'Comedy', 'Drama')]
imdb_genres_scores <- imdb_genres[, 
                                  list(n_titles = .N, votes = mean(avg_vote)), 
                                  by = list(year, genre)] 

theme_set(theme_classic())
```

Рисуем точками фильмы, которые выпустил Дэвид Линч. Задаем базовый объект с помощью `ggplot()` и на него наслаиваем геом `geom_point()`. Получаем стандартный график, где черным цветом отрисованы точки, а названия осей взяты из названий колонок.

```{r}
ggplot() +
  geom_point(data = imdb_lynch,
             mapping = aes(x = year, y = avg_vote))
```

<br>

##### geom_point() aesthetics
У `geom_point()` есть множество опций, которые позволяют задать визуальные параметры  (aesthetics) точки графика. Самые часто используемые:

- colour: цвет края точки (названимем или rgb-кодом)
- fill: цвет заливки точки (названимем или rgb-кодом)
- shape: форма точки, можно задать номером или по названию. 
- size: размер точки
- alpha: прозрачность точки

Типы точек по номерам:
```{r , echo=FALSE}
shapes <- data.frame(
  shape = c(0:19, 22, 21, 24, 23, 20),
  x = 0:24 %/% 5,
  y = -(0:24 %% 5)
)
ggplot(shapes, aes(x, y)) + 
  geom_point(aes(shape = shape), size = 5) +
  geom_text(aes(label = shape), hjust = 0, nudge_x = 0.15) +
  scale_shape_identity() +
  expand_limits(x = 4.1) +
  theme_void()
```

Типы точек по названиям:
```{r, echo=FALSE}
shape_names <- c(
  "circle", paste("circle", c("open", "filled", "cross", "plus", "small")), "bullet",
  "square", paste("square", c("open", "filled", "cross", "plus", "triangle")),
  "diamond", paste("diamond", c("open", "filled", "plus")),
  "triangle", paste("triangle", c("open", "filled", "square")),
  paste("triangle down", c("open", "filled")),
  "plus", "cross", "asterisk"
)

shapes <- data.frame(
  shape_names = shape_names,
  x = c(1:7, 1:6, 1:3, 5, 1:3, 6, 2:3, 1:3),
  y = -rep(1:6, c(7, 6, 4, 4, 2, 3))
)

ggplot(shapes, aes(x, y)) +
  geom_point(aes(shape = shape_names), size = 5) +
  geom_text(aes(label = shape_names), nudge_y = -0.3, size = 3.5) +
  scale_shape_identity() +
  theme_void()
```

<br>

##### Настройка визуальных параметров

Визуальные параметры можно задать двумя методами. Первый - когда форма, цвет или размер точки задаются не пользователем, а берутся из значений колонки в датасете. В таком случае визуальный параметр задается в аргументе `mapping`.

Повторим предыдущий график (фильмы Линча), и в `mapping` укажем, что цвет и форма точки задаются по значениям из колонки `country`, а размер точки берется из значений в колонке `duration`. 
```{r}
ggplot() +
  geom_point(data = imdb_lynch,
             mapping = aes(x = year, y = avg_vote,
                           color = country, shape = country, size = duration))  
```


Второй метод настройки визуальных параметров - когда значение параметра задается прямо в коде, конкретным значением. И этот параметр будет применен ко всем точкам графика. В таком подходе параметры задаются не в аргументе `mapping`, а как отдельные аргументы функции геома (`geom_point()`).

На графике ниже все точки, маркирующие фильмы Дэвида Линча, покрашены в красный цвет и сделаны ромбами, независимо от страны или еще каких-то других особенностей фильма. А вот размер точки все также зависит от значений в колонке `duration`:

```{r}
ggplot() +
  geom_point(data = imdb_lynch,
             mapping = aes(x = year, y = avg_vote),
             color = 'red', shape = 'diamond', size = 4)
```

<br>

<!-- ##### Выделение групп точек -->

<!-- Возьмем датасет `imdb_wm` - фильмы двух режиссеров, Линч Аллена и Мартина Скорсезе. Зададим цвет точек в зависимости от того, какого режиссера это фильм. Размер точки также оставим зависящим от длительности фильма. -->
<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_point(data = imdb_wm,  -->
<!--              mapping = aes(x = year,  -->
<!--                            y = avg_vote,  -->
<!--                            size = duration, -->
<!--                            color = director_name), -->
<!--              shape = 'diamond') -->
<!-- ``` -->

<!-- <br> -->

#### geom_line()

Практически идентичный `geom_point()` по конструкции геом для отрисовки линий. В отличие от `geom_point()` предполагается, что одному значению по `x`соответствует одно значение по `y`. Либо их может быть несколько, но тогда должна так же быть группирующая переменная, которая позволит различать, что несколько значений по `y` для одного `x` принадлежат разным линиям.

Покажем динамику оценко фильмов жанра Horror:
```{r}
ggplot() +
  geom_line(data = imdb_genres_scores[genre == 'Horror'], 
            mapping = aes(x = year, y = votes))
```

Воспользуемся механизмом добавления слоев и добавим на график еще и точки:
```{r}
ggplot() +
  geom_line(
    data = imdb_genres_scores[genre == 'Horror'], 
    mapping = aes(x = year, y = votes)) + 
  geom_point(
    data = imdb_genres_scores[genre == 'Horror'],
    mapping = aes(x = year, y = votes))
```

<br>

##### geom_line() aesthetics

Основные визуальные параметры схожи с  параметрами точек, тип линии задается параметром `linetype`:

- colour: цвет линии (названием или rgb-кодом)
- linetype: тип линии
- size: толщина линии
- alpha: прозрачность линии

Типы линий можно задать как номером, так и названием:
```{r, echo=FALSE}
lty <- c("solid", "dashed", "dotted", "dotdash", "longdash", "twodash")
linetypes <- data.frame(
  y = seq_along(lty),
  lty = lty
) 
ggplot(linetypes, aes(0, y)) + 
  geom_segment(aes(xend = 5, yend = y, linetype = lty)) + 
  scale_linetype_identity() + 
  geom_text(aes(label = lty), hjust = 0, nudge_y = 0.2) +
  scale_x_continuous(NULL, breaks = NULL) + 
  scale_y_reverse(NULL, breaks = NULL) + 
  theme_void()
```

Пунтирные линии также можно задать кодом, код должен состоять из 2, 4, 6 или 8 символов шестнадцатиричной системы счисления, где цифры обозначают количество точек. Например, код `3519` означает 'три отрисуй, 5 пропусти, 1 отрисуй, 9 пропусти, повторяй в цикле'. В результате получается вот такой вот кастомный пунктир:
```{r}
ggplot() +
  geom_line(data = imdb_genres_scores, 
            mapping = aes(x = year, y = votes,
                          group = genre, color = genre, linetype = genre))
```

<br>

##### Настройка визуальных параметров

Аналогично `geom_point()`, можно указать конкретные значения, можно указать, что значения должны браться из определенной колонки датасета. Возьмем датасет по жанрам, чтобы можно было в зависимости от жанра указать тип и цвет линии:

```{r}
ggplot() +
  geom_line(data = imdb_genres_scores, 
            mapping = aes(x = year, y = votes,
                          group = genre, color = genre, linetype = genre))
```

Если указать эти параметры не в `mapping`, а задать конкретные значения, то изменены будут параметры всех линий, независимо от жанра:

```{r}
ggplot() +
  geom_line(data = imdb_genres_scores, 
            mapping = aes(x = year, y = votes,
                          group = genre, color = genre, linetype = genre), 
            size = 1.5)
```

<br>

### геомы визуальных акцентов

#### geom_vline()

Простейший геом, который позволяет с помощью вертикальной линии акцентировать внимание пользователя на какой-то части кода (vline - vertical line). Точка на оси OX, из которой выводится линия, задается с помощью аргумента `xintercept`. Этот геом не требует обязательного указания датасета и осей, так как не особо зависит от них - главное, чтобы ось OX была такого же типа, как задается `xintercept` (число, строка, дата).

Укажем на графике границу 2000 года, чтобы понять, какие фильмы Линч снял после 2010 года.

```{r}
ggplot() +
  geom_point(
    data = imdb_lynch,
    mapping = aes(x = year, y = avg_vote, 
                  color = country, shape = country, size = duration)) + 
  geom_vline(xintercept = 2010)
```

<br>

#### geom_hline()
Аналогично `geom_vline()` позволяет нанести на график горизонтальную линию. Для этого надо указать `yintercept` - значение на оси OY, из которого будет выводиться линия, параллельная OX. Графически отметим фильмы, которые имеют `avg_vote` больше 0. У Дэвида Линча был один фильм (согласно датасету), у которого оценка была заметно больше 8, и тот был задолго до 2000 года:
```{r}
ggplot() +
  geom_point(
    data = imdb_lynch,
    mapping = aes(x = year, y = avg_vote, 
                  color = country, shape = country, size = duration)) + 
  geom_vline(xintercept = 2010) + 
  geom_hline(yintercept = 8)
```

<br>

#### geom_text()

Еще один инструмент расстановки акцентов на график - нанесение текстовых меток значений. В самом простом виде `geom_text()` аналогичен геомам точек и линий, с единственным отличием - метки задаются с помощью аргумента `label` в `mapping`. В качестве источника значений для меток указывают колонку датасета, в нашем случае это `original_title`:

```{r}
ggplot() +
  geom_text(
    data = imdb_lynch,
    mapping = aes(x = year, 
                  y = avg_vote, 
                  label = original_title)
  )
```

<br>

##### geom_text() aesthetics

Для текстовых меток на графике есть ряд дополнительных параметров, помимо размера или цвета. Это семейство шрифта (sans, serif, mono) и тип выделения (обычный, жирный, курсив). Так как текстовые метки обычно приводятся для каких-то других элементов графиков (точек или линий), еще есть аргументы hjust и vjust, для сдвига по вертикали или по горизонтали относительно целевой координаты.

- alpha: прозрачность
- angle: угол (если надо разместить метку под углом к точке)
- colour: цвет
- hjust: сдвиг по вертикали
- vjust: сдвиг по погризонтали
- lineheight: межстрочный интервал
- size: размер
- family: семейство шрифта (с засечками, без засечек)
- fontface: тип выделения
- check_overlap: если TRUE, то текстовые метки размещаются без того, чтобы перекрывать друг друга

Значения для `family` и `fontface`:
```{r}
df_fontface <- data.frame(x = 1:4, fontface = c("plain", "bold", "italic", "bold.italic"))
df_family <- data.frame(x = 1:3, y = 3:1, family = c("sans", "serif", "mono"))

ggplot() + 
  geom_text(data = df_fontface, 
            mapping = aes(x = x, y = 0.1, label = fontface, fontface = fontface), 
            size = 10) + 
  geom_text(data = df_family, 
            mapping = aes(x = x, y = 0.2, label = family, family = family), 
            size = 10) + 
  lims(x = c(0.5, 4.5), y = c(0, 0.3)) + 
  theme_void()
```

<br>

#### Настройка визуальных параметров
Аналогично прочим геомам - можно задать параметры как констранту, можно использовать значения определенных колонок для упарвления цветом, раззмером и т.д. У `geom_text()` также есть ряд параметров, который не может быть связан со значениями колонок - это запрет на пересечение текстовых меток (`check_overlap`) и смещение по вертикали/горизонтали (`hjust`, `vjust`). Сами по себе текстовые метки не очень инетерсны, поэтому также накладываем слой точек:

```{r}
ggplot() +
  geom_point(
    data = imdb_lynch,
    mapping = aes(x = year, y = avg_vote, size = duration),
    color = 'steelblue') + 
  geom_text(
    data = imdb_lynch, 
    mapping = aes(x = year, y = avg_vote, label = original_title),
    check_overlap = TRUE,
    hjust = -0.1
  )
```

<br>

<!-- ### Статистические геомы -->

<!-- #### geom_bar() -->
<!-- Геом используется для отрисовки столбиковых диаграмм (барчартов). При использовании геома по оси ОХ указывают переменную категорий (даты, строковые значения и проч). У геома есть, среди прочи, есть важный параметр, `stat`. -->

<!-- Когда `stat = 'count'` (это значение по умолчанию), то ось OY указывать не надо. Так как при отрисовке графика происходит вычисление количества наблюдений (строк) на каждое значение по оси OX, и именно по этому вычисленному значению отрисовываются столбики. (предупреждение `Removed 108 rows containing non-finite values (stat_count). ` означает, что есть пропуски в колонке `year`) -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_bar(data = imdb,  -->
<!--            mapping = aes(x = year),  -->
<!--            stat = 'count') -->
<!-- ``` -->

<!-- Когда `stat = 'identity'` (это значение надо пропиcывать), то необходимо указать и по значениям какой колонки строится ось OY. Этот вариант предпочтительнее в работе - лучше сначала явно создать отдельный объект, а потом его отрисовать, чем делать неявные вычисления при отрисовке графика. -->

<!-- ```{r} -->
<!-- # считаем количество строк в каждый год -->
<!-- imdb_ntitles  <- imdb[, .N, by = year] -->
<!-- imdb_ntitles[1:3] -->

<!-- # указываем год как x, а количество строк как y -->
<!-- ggplot() + -->
<!--   geom_bar(data = imdb_ntitles,  -->
<!--            mapping = aes(x = year, y = N),  -->
<!--            stat = 'identity') -->
<!-- ``` -->

<!-- <br> -->

<!-- ##### geom_bar() aesthetics -->

<!-- Основные параметры: -->

<!-- - colour: цвет края столбика (названимем или rgb-кодом) -->
<!-- - fill: цвет заливки столбика (названимем или rgb-кодом) -->
<!-- - alpha: прозрачность заливки столбика -->
<!-- - position: если групп несколько, то как должны быть организвоаны столбики по группам - "стопкой" или "рядом" -->

<!-- <br> -->

<!-- ##### Настройка визуальных параметров -->

<!-- Как и во всех геомах, можно использовать колонки датасета для того, чтобы задать цвет контура или заливки столбика. Обычно делается только в том случае, когда есть разные группы в датасете, и их надо выделить цветом. Во всех остальных случаях цвета контура и заливки, как правило, задаются фиксировнными значениями: -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_bar(data = imdb_ntitles,  -->
<!--            mapping = aes(x = year, y = N),  -->
<!--            stat = 'identity', -->
<!--            color = 'darkblue', -->
<!--            fill = 'steelblue', -->
<!--            alpha = 0.1) -->
<!-- ``` -->

<!-- Группировка может быть двух видов - "стопкой", когда в столбике на одном значении OX выделены цветом группы. Это значение по умолчанию, и задается с помощью параметра `position - 'stack'`. Отрисуем, сколько фильмов Линч Аллен и Мартин Скорсезе сняли в разных странах. Цвета заливки и контура столбика задаем по значениям колонки country (каждая страна выделяется одним цветом): -->

<!-- ```{r} -->
<!-- # считаем количество фильмов по странам и режиссерам -->
<!-- imdb_wm_groups <- imdb_wm[, .N, by = list(director_name, country)] -->
<!-- imdb_wm_groups -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_bar( -->
<!--     data = imdb_wm_groups, -->
<!--     mapping = aes(x = director_name, y = N, color = country, fill = country), -->
<!--     stat = 'identity', -->
<!--     position = 'stack' -->
<!--   ) -->
<!-- ``` -->

<!-- Тот же самый график, но с группировкой "рядом": -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_bar( -->
<!--     data = imdb_wm_groups, -->
<!--     mapping = aes(x = director_name, y = N, color = country, fill = country), -->
<!--     stat = 'identity', -->
<!--     position = 'dodge' -->
<!--   ) -->
<!-- ``` -->

<!-- <br> -->

<!-- #### geom_histogram() -->
<!-- Гистограммы во многом похожи на барчарты c `stat = 'count'`, с единственным отличием. Барчарты строятся на дискретных значения оси OX (имя режиссера, дата и проч). А гистограммы строятся на интервальной шкале, то есть, позволяют самим выбирать, сколько значений по оси OX будет в одном столбике. Это задается параметром `binwidth`: -->

<!-- Построим гистограмму по количеству фильмов с шагом пять лет: -->
<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_histogram( -->
<!--     data = imdb, -->
<!--     aes(x = year), -->
<!--     binwidth = 5) -->
<!-- ``` -->

<!-- Построим гистограмму по количеству фильмов с шагом в 1 год, это должно быть идентично `geom_bar()`: -->
<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_histogram( -->
<!--     data = imdb, -->
<!--     aes(x = year), -->
<!--     binwidth = 1) -->
<!-- ``` -->

<!-- <br> -->

<!-- #### geom_boxplot() -->

<!-- Геом для отрисовки боксплотов, по конструкции похож на `geom_bar()`. Отрисуем боксплоты распределений оценок фильмов Дэвида Линча и Мартина Скорсезе: -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_boxplot( -->
<!--     data = imdb_wm, -->
<!--     mapping = aes(x = director_name,  -->
<!--                   y = avg_vote)) -->
<!-- ``` -->

<!-- Укажем, чтобы цвет заливки и контура боксплота были свои для каждого режиссера. Так как заливка в чистом виде непрозрачная, необходимо отдельно указать параметр `alpha`, который задаст степень прозрачности заливки: -->
<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_boxplot( -->
<!--     data = imdb_wm, -->
<!--     mapping = aes(x = director_name,  -->
<!--                   y = avg_vote,  -->
<!--                   colour = director_name, -->
<!--                   fill = director_name), -->
<!--     alpha = 0.1) -->
<!-- ``` -->

<!-- <br> -->

<!-- #### geom_smooth() -->

<!-- Геом задает линии тренда. Как правило, линии задаются формулой `y ~ x`, где значения по y - зависимая, а x - независимая, предиктор. Методов построения линии может быть несколько, не только `lm` и `loess` (линейная регрессия и локальная полиномиальная регрессия соответственно), подробнее в справке по геому в аргументе `method`. -->

<!-- Построим по облаку точек фильмов из жанра `Fantasy` тренд imdb-оценок. Нарисуем сначала собственно облако точек, а потом зададим линию тренда с помощью `loess`.  -->

<!-- ```{r} -->
<!-- imdb_fantasy <- imdb[grep('Fantasy', genres)] -->
<!-- imdb_fantasy <- imdb_fantasy[!is.na(year)] -->

<!-- ggplot() + -->
<!--   geom_point(data = imdb_fantasy, -->
<!--              mapping = aes(x = year, y = avg_vote)) +  -->
<!--   geom_smooth(data = imdb_fantasy, -->
<!--              mapping = aes(x = year, y = avg_vote), -->
<!--              formula = y ~ x, method = 'loess') -->
<!-- ``` -->

<!-- <br> -->

<!-- ##### geom_smooth() aesthetics -->

<!-- Визуальные параметры геома в первую очередь включают в себя параметры линии, а также параметры доверительного интервала: -->

<!-- - colour: цвет линии -->
<!-- - fill: цвет заливки области доверительного интервала -->
<!-- - alpha: прозрачность заливки области доверительного интервала -->
<!-- - se: если se = FALSE, то доверительный интервал скрывается -->

<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_point(data = imdb_fantasy, -->
<!--              mapping = aes(x = year, y = avg_vote)) +  -->
<!--   geom_smooth(data = imdb_fantasy, -->
<!--              mapping = aes(x = year, y = avg_vote), -->
<!--              formula = y ~ x, method = 'loess',  -->
<!--              color = 'steelblue',  -->
<!--              fill = 'steelblue',  -->
<!--              alpha = 0.2) -->
<!-- ``` -->

<!-- <br> -->

<!-- ##### Настройка визуальных параметров -->

<!-- Как и со времи геомами, можно линии тренда формировать в соответствии с значениями в колонках датасета. Например, можно одновременно нарисовать две линии тренда, для каждого жанра. -->

<!-- Построим датасет, в котором будут фильмы жанров Fantasy и Horror. Пренебрежем ситуацией, когда фильм входит в оба жанра: -->

<!-- ```{r} -->
<!-- imdb_genres <- imdb[grep('Fantasy|Horror', genres)] -->
<!-- imdb_genres[grep('Fantasy', genres), genre := 'Fantasy'] -->
<!-- imdb_genres[grep('Horror', genres), genre := 'Horror'] -->
<!-- imdb_genres <- imdb_genres[!is.na(year)] -->
<!-- imdb_genres[, .N, by = genre] -->
<!-- ``` -->

<!-- Попробуем нарисовать точками фильмы трех жанров и построить по ним линии трендов оценок. Цвет и заливку задаем зависящими от значений жанра, прозрачность общая. Также делаем третий тренд - общий тренд по двум жанрам, и для него скрываем доверительный интервал. -->
<!-- ```{r} -->
<!-- ggplot() + -->
<!--   geom_point(data = imdb_genres, -->
<!--              mapping = aes(x = year, y = avg_vote, color = genre)) +  -->
<!--   geom_smooth(data = imdb_genres, -->
<!--              mapping = aes(x = year, y = avg_vote, color = genre, fill = genre), -->
<!--              formula = y ~ x,  -->
<!--              method = 'loess',  -->
<!--              alpha = 0.2) +  -->
<!--   geom_smooth(data = imdb_genres, -->
<!--              mapping = aes(x = year, y = avg_vote), -->
<!--              formula = y ~ x, method = 'loess',  -->
<!--              color = 'darkblue',  -->
<!--              se = FALSE) -->
<!-- ``` -->

<!-- <br> -->

### Композиция слоев

Если внимательно посмотреть на график, на котором были бы точками отмечены фильмы режиссера и текстовые метки названий, то видно много повторяющихся элементов - аргументы `data` и частично `mapping` в каждом геоме.

```{r}
ggplot() +
  geom_point(
    data = imdb_lynch,
    mapping = aes(x = year, y = avg_vote, size = duration),
    color = 'steelblue') + 
  geom_text(
    data = imdb_lynch, 
    mapping = aes(x = year, y = avg_vote, label = original_title),
    check_overlap = TRUE,
    hjust = -0.1
  )
```

Можно сократить количество кода, воспользовавших логикой наследования параметров геомов от основного `ggplot()`. Таким образом, датасет и параметры осей можно указать в `ggplot()`, и они будут применяться ко всем наслаиваемым геомам. А в геомах можно оставить только параметры этого конкретного геома.

```{r}
ggplot(data = imdb_lynch, mapping = aes(x = year, y = avg_vote)) +
  geom_point(mapping = aes(size = duration), color = 'steelblue') + 
  geom_text(mapping = aes(label = original_title), check_overlap = TRUE, hjust = -0.1)
```

Если сокращать еще больше, то можно опустить указание аргументов, и воспользоваться тем, что в `ggplot()` первый аргумент задает датасет, втором - метод использования колонок (`mapping`). В геомах наоборот, сначала указывается `mapping`:
```{r}
ggplot(imdb_martin, aes(x = year, y = avg_vote)) +
  geom_point(aes(size = duration), color = 'steelblue') + 
  geom_text(aes(label = original_title), check_overlap = TRUE, hjust = -0.1)
```

Наследование параметров не отменяет того, что на график можно наложить данные другого датасета (если параметры осей совпадают), для этого надо также, как и ранее, указать датасет в геоме. Наложим на точечный график фильмов Дэвида Линча фильмы Мартина Скорсезе и покрасим их красным:

```{r}
ggplot(imdb_lynch, aes(x = year, y = avg_vote)) +
  geom_point(aes(size = duration), color = 'steelblue') + 
  geom_text(aes(label = original_title), check_overlap = TRUE, hjust = -0.1) + 
  geom_point(
    data = imdb_martin, 
    color = 'red'
  )
```


## Дополнительные материалы

[Документация](https://ggplot2.tidyverse.org/) по пакету, есть примеры.

[Шпаргалки](https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf) - короткие и наглядные справочные материалы по основам синтаксиса и базовым геомам

[R cookbook](http://www.cookbook-r.com/Graphs/) - сборник примеров и кейсов, как решать наиболее часто встречающиеся задачи при работе с ggplot. Сгруппировано по разделам.

[Список названий цветов](http://www.kenstoreylab.com/wp-content/uploads/2015/08/colorbynames.png)

Elegant Graphics for Data Analysis (Use R!) - книга автора ggplot2, с очень внятным описанием базовых идей. При желании, можно найти в сети.

## Домашнее задание

Все задания выполняйте с учетом логики композиции слоев (не надо в каждом геоме писать датасет).

### Задание 1
С помощью пакета `ggplot2` отрисуйте график рассеяния, отражающий связь таких параметров, как `carat` и `price`. Используйте уже доступный после установки `ggplot2` датасет `diamonds`, сделайте выборку на 10000 строк (используйте `set.seed(1234)` для генерации зерна генератора случайных цифр). Удалите строки, в которых `carat > 3`. Сабсет назовите `diamonds_sample`. Для конвертации `diamonds` в data.table вам поможет `as.data.table()`

### Задание 2
Повторите предыдущий график, добавьте выделение цветом бриллиантов разного качества (`cut`).

### Задание 3
Добавьте на график из задания 2 вертикальную линию (OX = 2) и горизонтальную линию (OY = 15000).

### Задание 4
Выделите цветами из образованных вертикальной и горизонтальной линиями секторов первый и третий секторы (счет против часовой). Используйте зеленый и красный цвета соответственно, с параметром прозрачности opacity = 0.1. Сами линии можно не рисовать. Вам потребуется выбрать необходимый геом для решения этой задачи.

### Задание 5

Модифицируйте предыдущий график - сделайте точки во втором и четвертом секторах черными. 


